<?php session_start() ?>
<!--DB-->
<?php
$_title2 = 'About';
include 'conf/getGeneral.php';
?>
<!DOCTYPE html>
<html>
    <?php include 'inc/head.inc.php'; ?>
    <body>
        <?php include 'pages/header.php' ?>

        
        <aside id="fh5co-hero" class="js-fullheight">
            <div class="flexslider js-fullheight">
                <ul class="slides">
                <li style="background-image: url(images/customer/รูปลูกค้า_181007_0053.jpg);">
                    <div class="overlay-gradient"></div>
                    <div class="container">
                        <div class="col-md-10 col-md-offset-1 text-center js-fullheight slider-text">
                            <div class="slider-text-inner desc">
                                <h2 class="heading-section">About Us</h2>
                                <p class="fh5co-lead">Your Welcome <a href="contact" style="color:Yellow;">Happy Taxi Phuket</a></p>
                            </div>
                        </div>
                    </div>
                </li>
                </ul>
            </div>
        </aside>

        
        <div id="fh5co-content">
            <div class="video fh5co-video" style="background-image: url(images/index_profile.jpg);">
            </div>
            <div class="choose animate-box">
                <div class="fh5co-heading">
                    <h2>Why Choose Us?</h2>
                    <p>support fast development of tourism industry in Phuket. Since we have found lack of high quality transportation facility, we decided to launch new company which concentrates on providing quality and professional services. With our experiences and customer service knowledge, we could meet client’s expectation and be a part of impression for traveler, family, group tour and many organizations till present. </p>
                </div>
            </div>
        </div>

        
        <div id="fh5co-project" class="fh5co-bg-section">
            <div class="container fix">
                <div class="row animate-box">
                    <div class="col-md-8 col-md-offset-2 text-center fh5co-heading">
                        <h2>Favorite Photo</h2>
                        <p>"Phuket, 'Pearl of Andaman' is Thailand's largest island. With its magnificent white sandy beaches and bay, its crystal blue water, its hospitable and friendly people has never failed to impress visitors, Phuket truly offers a memorable holiday.</p>
                    </div>
                </div>
            </div>
            <div class="container">
                <div class="row">
                    <div class="col-md-4 col-sm-6 text-center fh5co-project animate-box" data-animate-effect="fadeIn">
                        <a ><img src="images/customer/รูปลูกค้า_181007_0009.jpg" alt="Can not find image Happy Taxi Phuket" class="img-responsive ismax">
                        </a>
                    </div>
                    
                    <div class="col-md-4 col-sm-6 text-center fh5co-project animate-box" data-animate-effect="fadeIn">
                        <a ><img src="images/customer/รูปลูกค้า_181007_0092.jpg" alt="Can not find image Happy Taxi Phuket" class="img-responsive ismax">
                        </a>
                    </div>

                    
                    <div class="col-md-4 col-sm-6 text-center fh5co-project animate-box" data-animate-effect="fadeIn">
                        <a ><img src="images/customer/รูปลูกค้า_181007_0104.jpg" alt="Can not find image Happy Taxi Phuket" class="img-responsive ismax">
                        </a>
                    </div>
                    
                    <div class="col-md-4 col-sm-6 text-center fh5co-project animate-box" data-animate-effect="fadeIn">
                        <a ><img src="images/customer/รูปลูกค้า_181007_0107.jpg" alt="Can not find image Happy Taxi Phuket" class="img-responsive ismax">
                        </a>
                    </div>
                    
                    <div class="col-md-4 col-sm-6 text-center fh5co-project animate-box" data-animate-effect="fadeIn">
                        <a ><img src="images/customer/55.jpg" alt="Can not find image Happy Taxi Phuket" class="img-responsive ismax">
                        </a>
                    </div>
                    
                    <div class="col-md-4 col-sm-6 text-center fh5co-project animate-box" data-animate-effect="fadeIn">
                        <a ><img src="images/customer/รูปลูกค้า_181007_0056.jpg" alt="Can not find image Happy Taxi Phuket" class="img-responsive ismax">
                        </a>
                    </div>

                </div>
            </div>
        </div>

        <div id="fh5co-started" style="background-image:url(images/img_bg_2.jpg);">
            <div class="overlay"></div>
            <div class="container">
                <div class="row animate-box">
                    <div class="col-md-8 col-md-offset-2 text-center fh5co-heading">
                        <h2>Happy Taxi Phuket</h2>
                        <p>To be ensured that I know all interesting places and everything in Phuket.<br>
                            This work is a challenge to me to have more opportunity meeting various travellers from many countries around the world.<br>
                            Trust in me, I am able to take care of you all with a friendship but local price as if you all are my family. 
                            I would promise you with all my heart and everything I am to give you a happiness and a safety, I believe you will be coming back to me again and again as well as teling this happinees in Phuket through your friends trying to visit Phuket one time.<br>
                            Phuket is my home, I would really like to announce world what Phuket has.<br/>
                            Please give me a chance to be your professional assistant to look after and take care of you all.<br>
                        </p>
                    </div>
                </div>
                <div class="row animate-box">
                    <div class="col-md-8 col-md-offset-2 text-center">
                        <p><a href="contact" class="btn btn-default btn-lg">Contact me</a></p>
                    </div>
                </div>
            </div>
        </div>

        <?php include 'pages/footer.php'; ?>
        <?php include 'inc/jsfoot.inc.php'; ?>
    </body>
</html>