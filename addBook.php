<?php
session_start();

/** Validate captcha */
if (!empty($_REQUEST['captcha'])) {
    if (empty($_SESSION['captcha']) || trim(strtolower($_REQUEST['captcha'])) != $_SESSION['captcha']) {
        echo "invalidcaptcha";
        exit();
    } else {
        if ($_SERVER['REQUEST_METHOD'] === 'POST') {
            include 'conf/config.php';
            include 'conf/function.php';
            include 'conf/getGeneral.php';
            require 'inc/phpMailer/PHPMailerAutoload.php';
            $tour_id = $_POST['tour_id'];

            //get tour name 
            $tour = select('tour', "where id = $tour_id", 'name,adult_net_price,child_net_price');
            $tour_name = $tour['name'];
            $adult_net_price = $tour['adult_net_price'];
            $child_net_price = $tour['child_net_price'];

            $firstname = $_POST['firstname'];
            $lastname = $_POST['lastname'];
            $phone = $_POST['phone'];
            $email = $_POST['emil'];
            $token = token(10);
            $hotelname = $_POST['hotelname'];
            $flightnumber = $_POST['flightnumber'];
            $destinations = $_POST['destinations'];
//            $passenger = $_POST['passenger'];
            $adult = $_POST['adult'];
            $child = $_POST['child'];

            $ct= $_POST['ct'];
            $roomnumber = $_POST['roomnumber'];

            $total_price_adult = $adult_net_price * $adult;
            $total_price_child = $child_net_price * $child;
            $total_price = $total_price_adult + $total_price_child;

            $nationality = $_POST['nationality'];
            $pickuptime = $_POST['pickuptime'];
            $remark = $_POST['remark'];
            $datenow = date("Y-m-d H:i:s");
            $bookid = 'BT' . $tour_id . date("dHis");
            $ip = $_SERVER['REMOTE_ADDR'];
            $fullcompanyname = $_webname . ' ' . $_webaddress . ' Tel.' . $_webtel . ' E-mail:<a href="mailto:' . $_webmail . '">' . $_webmail . '</a>';

            if($tour_id==9999){
            	$tour_name = "TRANSFER SERVICES";
            }

            $msg = '<table width="100%">
    <tbody>
    <tr>
        <td> <img style="width: 150px;" src="' . $_mailLogo . '"></td>
    </tr>
        <tr style="font-size: 20px">
            <td  class="bold" colspan="2">
                <div style="float:left">
                    Booking ID :   <span>' . $bookid . '</span>
                </div>
               
            </td>
        </tr>
        <tr>
            <td  class="bold">
                Customer Name : 
            </td>
            <td>
                ' . $firstname . ' ' . $lastname . '
            </td>
        </tr>
         <tr>
            <td  class="bold">
               Nationality : 
            </td>
            <td>
                ' . $nationality . '
            </td>
        </tr>
        <tr>
            <td  class="bold">
                E-mail : 
            </td>
            <td>
                ' . $email . '
            </td>
        </tr>
        <tr>
            <td  class="bold">
                Phone : 
            </td>
            <td>
                ' . $ct . ' '  . $phone . '
            </td>
        </tr>
        <tr style="background-color: blue">
            <td colspan="2"></td>
        </tr>
        <tr>
            <td  class="bold">
                Tour Name : 
            </td>
            <td>
                ' . $tour_name . '
            </td>
        </tr>
        
        <tr>
            <td  class="bold">
                Pick-Up Date & Time: 
            </td>
            <td>
                ' . $pickuptime . '
            </td>
        </tr>
       
            <tr>
                <td  class="bold">
                    Hotel Name / Pick-Up: 
                </td>
                <td>
                    ' . $hotelname . '
                </td>
            </tr>

             <tr>
                <td  class="bold">
                    Room Number: 
                </td>
                <td>
                    ' . $roomnumber . '
                </td>
            </tr>

            ';
            if($tour_id==9999){
                $msg.= '<tr>
                <td  class="bold">
                    Destinations : 
                </td>
                <td>
                    ' . $destinations . '
                </td>
            </tr> 
            <tr>
                <td  class="bold">
                    Flight Number: 
                </td>
                <td>
                    ' . $flightnumber. '
                </td>
            </tr>';
            }

             $msg.= '<tr>
                <td  class="bold">
                    Remark: 
                </td>
                <td>
                    ' . $remark . '
                </td>
            </tr>
       <tr>
            <td  class="bold">
                Passenger : 
            </td>
            <td>
                
            <table  width="100%" border="0" style="border:solid red 1px">
                <tr style="    border-bottom: 1px solid red;">
                   <th>Adult</th>
                   <th>Child</th>
                </tr>';
 if($tour_id==9999){
    $msg.= '
                <tr>
                    <td style="text-align:center;    border-bottom: 1px solid red;"> ' . $adult . ' </td>
                    <td style="text-align:center;border-bottom: 1px solid red;"> ' . $child . ' </td>
                </tr>
                <tr align="left" > 
                 <td colspan="2"></td>
                ';
 }else{
    
    $msg.= '<tr>
                    <td style="text-align:center;"> ' . $adult . ' x ' . $adult_net_price . ' = ' . $total_price_adult . ' </td>
                    <td style="text-align:center;border-left:1px solid red;"> ' . $child . ' x ' . $child_net_price . ' = ' . $total_price_child . ' </td>
                </tr>
                <tr align="right" >
                <td colspan="2">
                   <!-- <strong>Total: </strong> ' . $total_price . '
                       --></td>';

 }
                $msg.= ' </tr> </table>';

 if($tour_id==9999){
              $msg.= '<strong style="font-size:16px;color:red">Thank you very much for your email. I will let\'s you know price immediately.</strong>';
          }

            $msg.= '</td>
        </tr>
    </tbody>
</table> 
<!-- <div align="center">
    Please Click <a style="color:red;" href="http://www.phuketseeyoutravel.com/payment.php?id=' . $r['bookID'] . '">This</a> for pay your booking via Paypal (VISA/MASTER CARD NO PAYPAL ACCOUNT NEEDED)
</div> -->
<br/><br/>
<div align="center">
   <strong>' . $fullcompanyname . '</strong><br/> ' . $_domain . '
</div>';

            $subject = 'Booking Tours Bill #' . $bookid;
            
            $replayto = $_emailAddress;
            $to = $email; 
            for($i=0;$i<=1;$i++){
                if($i===1){
                     $replayto = $email;
                     $to = $_emailAddress; 
                }

            $headers= "From: Friendly Taxi & Tours Phuket <info@friendlytaxiphuket.com>\r\n";    
            // $headers = "From: " . $_webname . " <" . $_emailAddress . ">\r\n";
            $headers .= "Reply-To: " . strip_tags($replayto) . "\r\n";
            // $headers .= "CC: " . $_emailAddress . "\r\n";
            $headers .= "MIME-Version: 1.0\r\n";
            $headers .= "Content-Type: text/html; charset=UTF-8\r\n";
            $to = $to;
            $emailsend = @mail($to, $subject, $msg, $headers);
            if ($emailsend) {
                //return TRUE;
            } else {
                echo "err";
                exit();
            }
        }
           // $mail = new PHPMailer;
           // $mail->isSMTP();                                      // Set mailer to use SMTP
           // $mail->Host = $_emailhost;  // Specify main and backup SMTP servers
           // $mail->SMTPAuth = true;                               // Enable SMTP authentication
           // $mail->Username = $_emailUsername;                 // SMTP username
           // $mail->Password = $_emailPassword;                           // SMTP password
           // $mail->SMTPSecure = 'ssl';                            // Enable TLS encryption, `ssl` also accepted
           // $mail->Port = $_emailPort;
           // $mail->IsHTML(true);  // TCP port to connect to

           // $mail->setFrom($_emailAddress, 'FRIENDLY TAXI PHUKET');
           // $mail->addAddress($email,$firstname . ' ' . $lastname);     // Add a recipient
           // $mail->addAddress($_emailAddress);               // Name is optional
           // $mail->addReplyTo($_emailAddress);
           // $mail->Subject = $subject;
           // $mail->Body = $msg;
           // if (!$mail->send()) {
           //     echo 'Message could not be sent.';
           //     echo 'Mailer Error: ' . $mail->ErrorInfo;
           //     exit();
           // } else {
           //      // echo 'Message has been sent';
           // }
           
        } else {
            header("Location: index.php");
            exit();
        }
    }
    $request_captcha = htmlspecialchars($_REQUEST['captcha']);
    unset($_SESSION['captcha']);
}
?>