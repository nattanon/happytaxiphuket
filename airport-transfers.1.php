<?php session_start() ?>
<!--DB-->
<?php
$_title2 = 'Airport Transfers';
include 'conf/getGeneral.php';
?>
<!DOCTYPE html>
<html>
    <?php include 'inc/head.inc.php'; ?>
    <body>
        <a class="scrollToTop" href="#"><i class="fa fa-angle-up"></i></a>
        <div class="container">
            <div class="box_wrapper">
                <?php include 'pages/header.php' ?>
                <div class="latest_newsarea"></div>
                <section id="contentbody">
                    <div class="row">
                        <div class="col-lg-3 col-md-3 col-sm-4 col-xs-12">
                            <?php include 'pages/home_left.php'; ?>
                        </div>
                        <div class="col-lg-7 col-md-7 col-sm-8 col-xs-12">
                            <h2 class="post_title wow ">Taxi and airport transfers </h2>
                            <div class="entry">
                                <p>(Our Driver will be holding your name on a sign as you exit arrivals. </p>
                                <h3>No queuing for taxis, no stopping en route at tour booking agencies. </h3>
                                <br/>
                                <p>ALL transfers are operated on a private basis (i.e., you won't be sharing with others). 
                                    Prices PER VEHICLE, not per person. Be on your way to your hotel before the crowds leave the airport! 
                                    Please fill out the details below to view prices & book your transfer. Baby seat available on request. 
                                </p>
                                <div class="clear"></div>
                                <div style="padding-top: 20px"></div>
                                <img  class="img-responsive" src="images/friendly_tous.jpg" alt="Friendly Taxi Phuket" width="690" height="388" > 

                                <div class="clear"></div>
                                <div style="padding-top: 30px"></div>
                                <p>
                                    Contact me. Tell me where is your target or where is your hotel or where would you like to go and how many person? <br/>
                                    For last minute bookings, please call / whatsapp: <a href="tel:+66872679016">+66872679016</a><br/>
                                    <a href="contact">Click here to Contact </a>
                                </p>
                                
                                 <?php include 'inc/booking_form_transfers.php'; ?>
                            </div>
                        </div>
                        <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12">
                            <?php include 'pages/home_right.php'; ?>
                        </div>
                    </div>
                </section>
                <footer id="footer">
                    <?php include 'pages/footer.php'; ?>
                    <?php include 'pages/footer_copyright.php'; ?>
                </footer>
            </div>
        </div>
        <?php include 'inc/jsfoot.inc.php'; ?>
    </body>
</html>