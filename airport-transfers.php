<?php session_start() ?>
<!--DB-->
<?php
$_title2 = 'Airport Transfers';
error_reporting(E_ERROR | E_PARSE);
include 'conf/getGeneral.php';
?>
<!DOCTYPE html>
<html>
    <?php include 'inc/head.inc.php'; ?>
    <body> 
        <?php include 'pages/header.php' ?> 
        <aside id="fh5co-hero" class="js-fullheight">
            <div class="flexslider js-fullheight">
                <ul class="slides">
                <li style="background-image: url(images/bgat.jpg);">
                    <div class="overlay-gradient"></div>
                    <div class="container">
                        <div class="col-md-10 col-md-offset-1 text-center js-fullheight slider-text">
                            <div class="slider-text-inner desc">
                                <h2 class="heading-section">Airport Transfers</h2>
                                <p class="fh5co-lead">Enjoy and have fun with <span style="color:yellow;">Happy Taxi Phuket</span>.</p>
                            </div>
                        </div>
                    </div>
                </li>
                </ul>
            </div>
        </aside>
        
        
        <div id="fh5co-content" style="margin-bottom: 20px;" class="fh5co-bg-section">
            <div class="video fh5co-video" style="background-image: url(images/index_profile.jpg);">
            </div>
            <div class="choose animate-box">
                <div class="fh5co-heading">
                    <h2>Happy Taxi Phuket</h2>
                    <p>Our company provides for all of services. Our company arrange for all transportation. Our company have all types of vehicle, Minivan , Suv , sedans car . Please share all details to me and I will reply back immediately </p>
                    <p><b>Note: our company provide just only private transportation not sharing and direct to destination.</b></p>
                    <p>Contact me. Tell me where is your target or where is your hotel or where would you like to go and how many person? </p>
                    <p>For last minute bookings, please call / whatsapp: <a href="tel://0950197638">0950197638</a></p>
                    <a href="contact">Click here to Contact</a>
                
                </div>
            </div>
        </div>
        <div class="container" >
        
            <div class="row" >
                <?php include 'inc/booking_form_transfers.php'; ?>
            </div>
        </div>

        <div id="fh5co-started" style="background-image:url(images/img_bg_2.jpg);">
            <div class="overlay"></div>
            <div class="container">
                <div class="row animate-box">
                    <div class="col-md-8 col-md-offset-2 text-center fh5co-heading">
                        <h2>Happy Taxi Phuket</h2>
                        <p>To be ensured that I know all interesting places and everything in Phuket.<br>
                            This work is a challenge to me to have more opportunity meeting various travellers from many countries around the world.<br>
                            Trust in me, I am able to take care of you all with a friendship but local price as if you all are my family. 
                            I would promise you with all my heart and everything I am to give you a happiness and a safety, I believe you will be coming back to me again and again as well as teling this happinees in Phuket through your friends trying to visit Phuket one time.<br>
                            Phuket is my home, I would really like to announce world what Phuket has.<br/>
                            Please give me a chance to be your professional assistant to look after and take care of you all.<br>
                        </p>
                    </div>
                </div>
                <div class="row animate-box">
                    <div class="col-md-8 col-md-offset-2 text-center">
                        <p><a href="contact" class="btn btn-default btn-lg">Contact me</a></p>
                    </div>
                </div>
            </div>
        </div>

                                        
        
        
        
    	<?php include 'pages/footer.php'; ?>  
        <?php include 'inc/jsfoot.inc.php'; ?>
    </body>
    <script>
    	$( "#booking_form" ).submit(function( event ) {
    		var data = $( this ).serialize();
            var url = "inc/booking_airport_transfers.php";
            // var url = "inc/mailer.php";
    		console.log(data);
		 	$.ajax({
		        type: "post",
		        url: url,
		        dataType:"json",
		        data: data,
		        success: function (response) {
		            console.log(response);
		            if(response.status==0){
						alert(response.msg);
					}else{
						if(response.mail==true){
							alert("Success");
							location.reload();
						}
					}
		        }
		    });
		   /* $.post( url,data, function( response ) {
				 console.log(response);
			});*/
		  event.preventDefault();
		});
    </script>
</html>