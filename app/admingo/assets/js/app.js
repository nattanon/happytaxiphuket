$(document).ready(function () {
//    demo.initChartist();
//MENU 
    $('.leftmenu li').each(function () {
        $(this).removeClass('active');
        if (!getUrlParameter('pages')) {
            $('[data-menu="dashboard"]').addClass('active');
        } else {
            if ($(this).data('menu') === getUrlParameter('pages')) {
                $(this).addClass('active');
            }
        }
    });

    $('.paginate_button').addClass('btn');
    var table = $('.dataTable').DataTable();
    $('[data-btn]').click(function () {
        var $me = $(this);
        var link = $me.data('btnlink');
        if (link) {
            window.location = 'dashboard.php?pages=' + link;
            return;
        }
        var p = $me.data('btn');
        switch (p) {
            case  'addpackage':
                window.location = '?pages=tourAdd';
                break;
            case  'tourtype':
                window.location = '?pages=tourType';
                break;
//            case  'delete':
//                var id = $me.data('id');
//                var $model = $('#modelConfirm').appendTo("body").modal('show');
//                $model.find('[data-btn="delete_yes"]').on('click', function () {
//                    window.location = 'del.php?page=' + getUrlParameter('pages') + '&id=' + id + '';
//                });
//                break;
            case 'tourTypeAdd':
                var id = $me.data('id');
                var $model = $('#modelAddTourtype').appendTo("body").modal('show');
                if (id) {
                    getDataJson($model, id, 'tour_type');
                }
                break;


        }
    });

    $('.modal').on('hidden.bs.modal', function () {
        $(this).find('form')[0].reset();
        $(this).find('form').find('[name="id"]').val('');
    });
    $('[sys_pro]').each(function () {
        $(this).attr({
            'disabled': true,
            'title': 'Full Version Only!'
        });
    });
    $('.dataTable tbody').on('click', '[data-btn="tourAdd"]', function () {
        var id = $(this).data('id');
        window.location = '?pages=tourAdd&id=' + id + '';
    });
    $('.dataTable tbody').on('click', '[data-btn="delete"]', function () {
        var $me = $(this);
        var id = $me.data('id');
        var $model = $('#modelConfirm').appendTo("body").modal('show');
        $model.find('[data-btn="delete_yes"]').on('click', function () {
            window.location = 'del.php?page=' + getUrlParameter('pages') + '&id=' + id + '';
        });
    });

    //Set Active Menu
//    $('[data-name="menu"]').find('ul li').each(function () {
//        $(this).removeClass('active');
//        if ($(this).find('a').attr('href') === $(location).attr('search')) {
//            $(this).addClass('active');
//        }
//    });
//    var table = $('#datatables').DataTable();
//    $('#datatables tbody').on('click', 'tr', function () {
//        if ($(this).hasClass('selected')) {
//            $(this).removeClass('selected');
//        } else {
//            table.$('tr.selected').removeClass('selected');
//            $(this).addClass('selected');
//        }
//    });
//    $('#button').click(function () {
//        table.row('.selected').remove().draw(false);
//    });
//    $('[data-transfer-id]').click(function () {
//        var id = $(this).data('transfer-id');
//        $.dialog("[name='order_detail']", {
//            title: id
//        });
//    });

});
function makeid()
{
    var text = "";
    var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

    for (var i = 0; i < 50; i++)
        text += possible.charAt(Math.floor(Math.random() * possible.length));

    return text;
}
$(function () {
    //Function 
    $.fn.hasAttr = function (name) {
        return this.attr(name) !== undefined;
    };
    $('input').each(function () {

        if ($(this).hasAttr('title')) {
            console.log($(this));
            $(this).tooltip();
        }
    })


    //All page
//    $('[name="destination"]').find('span').each(function () {
//          if ($(this).hasAttr('title')) {
//            $(this).tooltip({
//                placement: 'top'
//            });
//        }
//    });
//    $('[data-link]').click(function () {
//        var link = $(this).data('link')
//        window.location = link;
//    });
//    $('[data-toggle="popover"]').popover();
//    $('[data-toggle="tooltip"]').tooltip();
//    $('[data-btn="book"]').click(function () {
//        var rateID = $(this).data('id');
//        var des = $(this).data('des');
//        var car = $('[name="car"]:checked').val();
//        window.location = 'phuket-transfers-' + rateID + '-' + car + '-' + des + '.html';
//        //window.location = 'booking.php?rate=' + rateID + '&car=' + car + '&topic=' + des + '';
//    });
//    $('[name="car"]').on('change', function () {
//        $('#passengers option').remove();
//        var max = 1;
//        if ($(this).val() === '1') { //2-3pax
//            max = 3;
//        } else if ($(this).val() === '2') { //4-10
//            max = 5;
//        } else { //4-10
//            max = 10;
//        }
//        for (var i = 1; i <= max; i++) {
//            $('#passengers').append($('<option>', {
//                value: i,
//                text: i
//            }));
//        }
//    });
//    $('[name="journey"]').on('change', function () {
//        if ($(this).val() === '2')
//            $('[name="returntime"]').attr('required', '').closest('div').slideDown('slow');
//        else
//            $('[name="returntime"]').removeAttr('required').closest('div').slideUp('slow');
//    });
//    $('[data-role="datetime"]').datetimepicker({
//        inline: false,
//        format: 'Y-m-d H:i',
//        zIndex: 9999
//    });
//    $('[data-role="getprice"]').change(function () {
//        var values = {
//            journey: $('[name="journey"]:checked').val(),
//            car: $('[name="car"]:checked').val(),
//            from: $('#from').val(),
//            destination: $('#to').val()
//        };
//        $.isLoading({text: "Loading"});
//        setTimeout(function () {
//            $.ajax({
//                type: 'POST',
//                url: 'getPrice.php',
//                data: values,
//                dataType: 'json',
//                success: function (data) {
//                    $('[name="cost"]').val(data.price);
////                    if (data.discount)
////                        $('#alert_discount').html('Discount <b>' + data.discount + '</b> THB ').slideDown('slow');
////                    else
//                    $('#alert_discount').html('').slideUp('slow');
//                    $.isLoading("hide");
//                },
//                error: function (jqXHR, textStatus, errorThrown) {
//                    console.log(jqXHR.responseText);
//                    console.log(textStatus, errorThrown);
//                    alert('error please contact admin');
//                }
//            });
//        }, 1000);
//    });
//    $('form[name="booking"]').submit(function () {
//        var $self = $(this);
//        $.isLoading({text: "Booking"});
//        setTimeout(function () {
//            $.ajax({
//                type: 'POST',
//                url: 'addBook.php',
//                dataType: 'html',
//                data: $self.serialize(),
//                success: function (r) {
//                    console.log(r);
//                    if (r === 'invalidcaptcha') {
//                        alert('Invalid captcha');
//                        $('#change-image').trigger('click');
//                        $.isLoading("hide");
//                        return;
//                    } else if (r === 'spam') {
//                        alert('You are spammer ! Get the @$%K out');
//                        return;
//                    }
//                    if (r !== 'error') {
//                        $.isLoading("hide");
//                        $.isLoading({text: "Booking Done! Please Wait "});
//                        setTimeout(function () {
//                            window.location = 'payment.php?id=' + r.trim() + '&tk=' + makeid() + '';
//                        }, 1500);
//                    }
//                }, error: function (jqXHR, textStatus, errorThrown) {
//                    console.log(jqXHR.responseText);
//                    console.log(textStatus, errorThrown);
//                    alert('error please contact admin');
//                }});
//        }, 1500);
//    });
//    $('#paypalpayment').submit(function () {
//        var f = document.getElementById('paypalpayment');
//        f.submit();
//    });
});