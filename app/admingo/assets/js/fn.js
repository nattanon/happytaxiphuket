var getUrlParameter = function getUrlParameter(sParam) {
    var sPageURL = decodeURIComponent(window.location.search.substring(1)),
            sURLVariables = sPageURL.split('&'),
            sParameterName,
            i;

    for (i = 0; i < sURLVariables.length; i++) {
        sParameterName = sURLVariables[i].split('=');

        if (sParameterName[0] === sParam) {
            return sParameterName[1] === undefined ? true : sParameterName[1];
        }
    }
};
var getDataJson = function ($this, id, table) {
    $.ajax({
        url: 'get/data.php',
        type: 'post',
        data: {id: id, p: table},
        dataType: 'json',
        success: function (data) {
            $this.set(data[0]);
        }, error: function (jqXHR) {
            console.log(jqXHR);
        }
    });
}
$.clearInput = function () {
    $('form').find('input[type=text], input[type=password], input[type=number],input[type=hidden], input[type=email], textarea').val('');
};