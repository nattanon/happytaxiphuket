$(function () {
    $('[name="frm_login"]').submit(function () {
        var data = $(this).serialize();
        $.ajax({
            url: 'checkuser.php',
            type: 'post',
            data: data,
            dataType: 'text',
            success: function (data) {
                if (data === 'yes') {
                    $.notify({
                        icon: 'ti-gift',
                        message: "Login done, Please Wait...",
                    }, {
                        type: 'success',
                        timer: 1000
                    });
                    setTimeout(function () {
                        window.location = "dashboard.php";
                    }, 1000);

                } else {
                    $.notify({
                        message: "User not found, Please check again.",
                    }, {
                        type: 'danger',
                        timer: 1000
                    });
//                    $('#usernotfound').slideDown(200).removeClass('hide');
//                    setTimeout(function () {
//                        $('#usernotfound').slideUp(200, function () {
//                            $(this).addClass('hide');
//                        });
//                    }, 1200);

                }
            }, error: function (jqXHR) {
                console.log(jqXHR);
            }
        });
    });
});