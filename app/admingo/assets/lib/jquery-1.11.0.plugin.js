window.jQuery && function ($) {
    var plugin = {
        params: function () {
            var params = {};
            for (var p = /([^&=]+)=?([^&]*)/g, q = location.search.substring(1), m, k; Boolean(m = p.exec(q)); ) {
                if (params[k = decodeURIComponent(m[1])] === undefined) {
                    params[k] = decodeURIComponent(m[2]);
                } else if (params[k] instanceof Array) {
                    params[k].push(decodeURIComponent(m[2]));
                } else {
                    params[k] = [params[k], decodeURIComponent(m[2])];
                }
            }
            return params;
        }(), browser: function () {
            var ua = navigator && navigator.userAgent.toLowerCase();
            var match = /(chrome)[ \/]([\w.]+)/.exec(ua)
                    || /(webkit)[ \/]([\w.]+)/.exec(ua)
                    || /(firefox)[ \/]([\w.]+)/.exec(ua)
                    || /(opera)(?:.*version|)[ \/]([\w.]+)/.exec(ua)
                    || /(msie) ([\w.]+)/.exec(ua)
                    || /(mozilla) ?(?:.*? rv:([\w.]+)|)/.exec(ua)
                    || [];
            if (match[1]) {
                var browser = {};
                browser[match[1]] = match[2] || "0";
                ua.indexOf("compatible") > 0 && (browser.compatible = true);
            }
        }(), cookie: function () {
            switch (arguments.length) {
                case 0:
                    var entry = document.cookie.split(';');
                    var object = {};
                    for (var i in entry) {
                        var value = entry[i].substring(entry[i].indexOf('=') + 1);
                        try {
                            value = JSON.parse(value);
                        } catch (e) {
                        }
                        object[entry[i].substring(0, entry[i].indexOf('='))] = value;
                    }
                case 1:
                    if (typeof arguments[0] === 'object') {
                        for (var i in arguments[0]) {
                            $.cookie(i, arguments[0][i]);
                        }
                    } else {
                        var entry = document.cookie.split(';');
                        for (var i in entry) {
                            if (entry[i].substring(0, entry[i].indexOf('=')) === arguments[0]) {
                                var value = entry[i].substring(entry[i].indexOf('=') + 1);
                                try {
                                    return JSON.parse(value);
                                } catch (e) {
                                    return value;
                                }
                            }
                        }
                    }
                    return;
                case 2:
                    if (arguments[1]) {
                        document.cookie = arguments[0] + '=' + JSON.stringify(arguments[1]);
                    } else {
                        document.cookie = arguments[0] + '=; expires=Thu, 01 Jan 1970 00:00:00 UTC';
                    }
            }
        }, i18n: {
            options: {},
            langs: {}
        }, parseJSON: function (data) {
            return Function("return " + data)();
        }, expr: {
            ':': {
                is: $.expr.createPseudo(function (arg) {
                    return function (ele) {
                        return $(ele).is(arg);
                    };
                }), search: $.expr.createPseudo(function (arg) {
                    arg = arg && arg.toUpperCase();
                    return function (ele) {
                        var text = ele.textContent || ele.innerText || $.find.getText(ele);
                        return text.toUpperCase().indexOf(arg) !== -1;
                    };
                }), value: $.expr.createPseudo(function (arg) {
                    return function (ele) {
                        return arg ? ele.value === arg : Boolean(ele.value);
                    };
                }), parents: $.expr.createPseudo(function (arg) {
                    return function (ele) {
                        return Boolean($(ele).parents(arg).length);
                    };
                })
            }
        }, fn: {
            listen: function (event) {
                for (var i in event) {
                    if (typeof event[i] === 'object') {
                        for (var j in event[i]) {
                            if (typeof event[i][j] === 'object') {
                                if (typeof event[i][j].fn === 'function') {
                                    this.on(j, i, event[i][j].data, event[i][j].fn);
                                } else {
                                    var l = [];
                                    var p = 0;
                                    var f;
                                    for (var k in i) {
                                        if (f) {
                                            if (i[k] === f) {
                                                f = undefined;
                                            }
                                        } else if ((i[k] === '"' || i[k] === "'") && i[k - 1] !== '\\') {
                                            f = i[k];
                                        } else if (i[k] === ',') {
                                            l.push(i.substring(p, k).trim());
                                            p = +k + 1;
                                        }
                                    }
                                    l.push(i.substring(p).trim());
                                    for (var k in l) {
                                        try {
                                            if (l[k]) {
                                                eval('this.listen({\'' + (l[k] + ' ' + j).replace(/'/g, '\\\'') + '\':event[i][j]})');
                                            }
                                        } catch (e) {
                                        }
                                    }
                                }
                            } else if (typeof event[i][j] === 'function') {
                                this.on(j, i, event[i][j]);
                            }
                        }
                    } else if (typeof event[i] === 'function') {
                        this.on(i, event[i]);
                    }
                }
                return this;
            }, inner: function (selector, fn) {
                if (typeof selector === 'function') {
                    if (fn) {
                        selector = selector.apply(this);
                    } else {
                        fn = selector;
                        selector = undefined;
                    }
                }
                typeof fn === 'function' && fn.apply(this, selector && [this.find(selector)]);
                return this;
            }, ineach: function (selector, fn) {
                if (typeof selector === 'function') {
                    if (fn) {
                        selector = selector.apply(this);
                    } else {
                        fn = selector;
                        selector = undefined;
                    }
                }
                (selector ? this.find(selector) : this).each(fn);
                return this;
            }, serializeArray: function () {
                var CRLF = /\r?\n/g,
                        submittable = /^(?:input|select|textarea|keygen)/i,
                        submitterTypes = /^(?:submit|button|image|reset|file)$/i,
                        checkableType = /^(?:checkbox|radio)$/i;
                return this.map(function () {
//                    var elements = $.prop(this, "elements");
//                    return elements ? $.makeArray(elements) : this;
                    return $.makeArray($.prop(this, "elements") || $(':input', this));
                }).filter(function () {
                    var type = this.type;
                    return this.name && !$(this).is(":disabled")
                            && submittable.test(this.nodeName)
                            && !submitterTypes.test(type)
                            && (this.checked || !checkableType.test(type));
                }).map(function (i, elem) {
                    // รอปรับรุงต่อ
                    if ($.data(elem, 'datepicker')) {
                        return {name: elem.name, value: $(elem).datepicker('getDate')};
                    }
                    var val = $(elem).val();
                    return val === null ? null
                            : $.isArray(val) ? $.map(val, function (val) {
                        return {name: elem.name, value: val.replace(CRLF, "\r\n")};
                    }) : {name: elem.name, value: val.replace(CRLF, "\r\n")};
                }).get();
            }, json: function () {
                var r = {};
                $.each(this.serializeArray(), function () {
                    if (r[this.name] === undefined) {
                        r[this.name] = this.value || null;
                    } else if (r[this.name] instanceof Array) {
                        r[this.name].push(this.value || null);
                    } else {
                        r[this.name] = [r[this.name], this.value || null];
                    }
                });
                return r;
            }, multipart: function () {
                return new FormData(this[0]);
            }, set: function () {
                if ($.type(arguments[0]) === 'object') {
                    for (var i in arguments[0]) {
                        this.set(i, arguments[0][i], Boolean(arguments[1]));
                    }
                } else if (arguments.length === 3
                        || (arguments.length === 2 && $.type(arguments[1]) !== 'boolean')) {
                    if ($.type(arguments[1]) === 'object') {
                        for (var i in arguments[1]) {
                            this.set(arguments[0] + '.' + i, arguments[1][i], Boolean(arguments[2]));
                        }
                    }
                    this.find('[name="' + arguments[0] + '"]:not([not-set]), [data-name="' + arguments[0] + '"]:not([not-set])')
                            .set(arguments[1], Boolean(arguments[2]));
                } else if (arguments[0] !== undefined) {
                    var _value = arguments[0] === null ? '' : arguments[0];
                    var flag = arguments[1];
                    this.each(function () {
                        var value = typeof _value === 'function' ? _value.call(this) : _value;
                        switch (this.nodeName.toUpperCase()) {
                            case 'INPUT':
                                if (value instanceof Date && $.data(this, 'datepicker')) {
                                    $(this).datepicker('setDate', value);
                                } else if (/checkbox|radio/i.exec(this.type)) {
                                    if (flag) {
                                        check(this.getAttribute('value'))
                                                ? this.setAttribute('checked', 'checked')
                                                : this.removeAttribute('checked');
                                    } else {
                                        this.checked = check(this.getAttribute('value'));
                                    }
                                    function check(val) {
                                        if (val === null || val === undefined) {
                                            return /^true$/i.test(value);
                                        } else if ($.type(value) === 'array') {
                                            for (var i in value) {
                                                if (String(value[i]) === val) {
                                                    return true;
                                                }
                                            }
                                            return false;
                                        } else {
                                            return String(value) === val;
                                        }
                                    }
                                } else if (!/file/i.exec(this.type)) {
                                    flag ? this.setAttribute('value', value) : this.value = value;
                                }
                                break;
                            case 'SELECT':
                                if (flag) {
                                    $(this).find('option').removeAttr('selected')
                                            .filter('[value="' + value + '"]')
                                            .attr('selected', 'selected');

                                } else {
                                    this.value = value;
                                }
                                break;
                            case 'TEXTAREA':
                                flag ? this.innerHTML = value : this.value = value;
                                break;
                            case 'TABLE':
                                if ($.type(value) !== 'array') {
                                    value = [value];
                                }
                                var $head = $('thead tr:first td', this);
                                var $body = $('tbody', this).html('');
                                $body.length || ($body = $('<tbody></tbody>').appendTo(this));
                                for (var i in value) {
                                    var html = '';
                                    html += '<tr>';
                                    $head.each(function () {
                                        var data = $(this).data();
                                        html += '<td';
                                        if (value[i][data.columnTitle]) {
                                            html += ' title="' + value[i][data.columnTitle] + '"';
                                        }
                                        html += '>';
                                        html += (value[i][data.columnName] || '');
                                        data.columnInput && $.each(data.columnInput.split(','), function (k, v) {
                                            html += '<input name="' + v + '" value="' + (value[i][v] || '') + '" type="hidden" />';
                                        });
                                        html += '</td>';
                                    });
                                    html += '</tr>';
                                    $body.append(html);
                                }
                                break;
                            default :
                                this.innerHTML = value;
                        }
                    });
                }
                return this;
            }, name: function (name) {
                return (name ? this.find('[name="' + name + '"], [data-name="' + name + '"]')
                        : this.find('[name], [data-name]')).filter(':not(:disabled)');
            }, dataname: function (subfix, name) {
                if (arguments.length < 2) {
                    name = subfix;
                    subfix = '';
                } else {
                    subfix = '-' + subfix;
                }
                return this.find('[data' + subfix + '="' + name + '"]');
            }, option: function () {
                var option = this.data('options');
                if (!(option instanceof window.Object)) {
                    option = {};
                }
                this[0] && this[0].attributes && $.each(this[0].attributes, function () {
                    if (this && this.name.indexOf('data-option-') === 0) {
                        var name = $.camelCase(this.name.slice(12));
                        try {
                            option[name] = $.parseJSON(this.value);
                        } catch (e) {
                            option[name] = this.value;
                        }
                    }
                });
                return option;
            }, wait: function (time, cb) {
                var _this = this;
                setTimeout(function () {
                    typeof cb === 'function' && cb.apply(_this);
                }, time);
                return this;
            }, i18n: function (lang) {
                var self = this;
                var opt = $.extend(true, {
                    cookie: '$.i18n.lang',
                    path: 'rs/js/ui/i18n',
                    ext: 'txt',
                    dfl: 'default'
                }, $.i18n.options, arguments[1]);
                if (lang) {
                    $.cookie(opt.cookie, lang);
                } else if (arguments.length === 0) {
                    lang = $.cookie(opt.cookie) || opt.dfl;
                } else {
                    $.cookie(opt.cookie, undefined);
                    lang = opt.dfl;
                }
                return queue(function () {
                    parallel(function () {
                        get_lang(lang, this.done);
                    }, function () {
                        lang === opt.dfl ? this.done() : get_lang(opt.dfl, this.done);
                    }).done(this.done);
                }).runon(function (lang, dfl) {
                    $.i18n.lang = lang = $.extend({}, dfl && dfl[0], lang && lang[0]);
                    $('[data-i18n]', self).andSelf('[data-i18n]').each(function () {
                        var $self = $(this);
                        var label = lang[$self.data('i18n')];
                        if (label) {
                            if ($self.is('img')) {
                                $self.attr('src', label);
                            } else if ($self.is('input')) {
                                $self.attr('placeholder', label);
                            } else {
                                $self.html(label);
                            }
                        } else {
                            if ($self.is('img')) {
                                $self.removeAttr('src');
                            } else if ($self.is('input')) {
                                $self.removeAttr('placeholder');
                            } else {
                                $self.html('');
                            }
                        }
                    });
                    return lang;
                });
                function get_lang(lang, cb) {
                    if ($.i18n.langs[lang]) {
                        typeof cb === 'function' && cb($.i18n.langs[lang]);
                    } else {
                        $.get(opt.path + '/' + lang + '.' + opt.ext).done(function (r) {
                            var o = {};
                            r.split(/\r?\n/).forEach(function (line) {
                                var separator = line.indexOf('=');
                                if (line.indexOf('#') === 0 || separator === -1) {
                                    return;
                                }
                                o[line.substring(0, separator)] = line.substring(separator + 1);
                            });
                            $.i18n.langs[lang] = o;
                            typeof cb === 'function' && cb(o);
                        }).fail(function () {
                            typeof cb === 'function' && cb();
                        });
                    }
                }
            }
        }
    };
    $.extend(true, plugin);
    $(function () {
        $('input[data-role="backed_detector"]').each(function () {
            if (this.value) {
                $(this).trigger('backed');
            } else {
                this.value = true;
            }
        });
    });
}(window.jQuery);
