$.dialog = function (element, option) {
    if (!(element instanceof $)) {
        element = $(element);
    }
    try {
        return element.$show().dialog(option = $.extend({
            height: 'auto',
            maxHeight: $(window).height() * 0.9,
            resizable: false,
            width: 'auto',
            hide: {effect: "fade", duration: 400},
            show: {effect: "fade", duration: 400},
            modal: true,
            closeAtModal: true,
            buttons: {
                Close: function () {
                    $(this).dialog("close");
                }
            },
            close: function () {
                element.dialog('destroy');
            }
        }, option)).listen({
            '[data-rel="close"]': {
                click: function () {
                    element.dialog('close');
                }
            }
        }).trigger('init');
    } finally {
        if (option.modal && option.closeAtModal) {
            $('.ui-widget-overlay').on('click', function () {
                element.dialog('close');
            });
        }
    }
};
$.extend($.fn, {
    $hide: function () {
        return this.addClass('hide');
    }, $show: function () {
        return this.removeClass('hide');
    }
});