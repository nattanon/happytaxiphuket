<?php
if (session_id() == '' || !isset($_SESSION)) {
    session_start();
    if (!isset($_SESSION['UserID'])) {
        header('Location: .');
    } else {
        $user_id = $_SESSION["UserID"];
    }
}
empty($_GET) ? $page = '' : $page = $_GET['pages'];
include '../../conf/config.php';
include '../../conf/getGeneral.php';
?>
<!doctype html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <link rel="apple-touch-icon" sizes="76x76" href="assets/img/apple-icon.png">
        <link rel="icon" type="image/png" sizes="96x96" href="assets/img/favicon.png">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
        <title>Go Travel System ระบบบริหารจัดการทัวร์ | <?= $_webname ?></title>
        <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
        <meta name="viewport" content="width=device-width" />
        <!-- Bootstrap core CSS     -->

        <!-- Animation library for notifications   -->
        <link href="assets/css/animate.min.css" rel="stylesheet"/>
        <link href="assets/lib/bootstrap/css/bootstrap.min.css" rel="stylesheet" />
        <link href="assets/lib/jasny-bootstrap/css/jasny-bootstrap.min.css" rel="stylesheet"/>
        <!--  Paper Dashboard core CSS    -->
        <link href="assets/css/paper-dashboard.css" rel="stylesheet"/>

        <link href="assets/lib/datatable/css/dataTables.bootstrap4.min.css" rel="stylesheet"/>
        <!--  CSS for Demo Purpose, don't include it in your project     -->
        <link href="assets/css/demo.css" rel="stylesheet" />

        <!--  Fonts and icons     -->
        <link href="http://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css" rel="stylesheet">
        <link href='https://fonts.googleapis.com/css?family=Muli:400,300' rel='stylesheet' type='text/css'>
        <link href="assets/css/themify-icons.css" rel="stylesheet"> 




        <script src="assets/js/jquery-1.10.2.js" type="text/javascript"></script>
        <script src="assets/lib/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
        <script src="assets/lib/ckeditor/ckeditor.js" type="text/javascript"></script>
        <!--  Checkbox, Radio & Switch Plugins -->
        <script src="assets/js/bootstrap-checkbox-radio.js"></script>
        <!--  Charts Plugin -->
        <script src="assets/js/chartist.min.js"></script>
        <!--  Notifications Plugin    -->
        <script src="assets/js/bootstrap-notify.js"></script>
        <!--  Google Maps Plugin    -->
        <!--<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js"></script>-->
        <!-- Paper Dashboard Core javascript and methods for Demo purpose -->
        <script src="assets/js/paper-dashboard.js"></script>
    </head>
    <body>
        <div class="wrapper">
            <div class="sidebar" data-background-color="white" data-active-color="danger">
                <!--
                            Tip 1: you can change the color of the sidebar's background using: data-background-color="white | black"
                            Tip 2: you can change the color of the active button using the data-active-color="primary | info | success | warning | danger"
                -->
                <div class="sidebar-wrapper">
                    <div class="logo">
                        <a href="http://www.creative-tim.com" class="simple-text">
                            Go Travel Sys<sup>2.0</sup>
                        </a>
                    </div>

                    <ul class="nav leftmenu">
                        <li data-menu="dashboard">
                            <a href=".">
                                <i class="ti-panel"></i>
                                <p>Dashboard</p>
                            </a>
                        </li>
                        <li data-menu="toursbooking">
                            <a href="?pages=toursbooking">
                                <i class="ti-bag"></i>
                                <p>Tours Booking</p>
                            </a>
                        </li>
                        <li data-menu="transfersbooking">
                            <a href="?pages=transfersbooking">
                                <i class="ti-car"></i>
                                <p>Transfers Booking</p>
                            </a>
                        </li>
                        <li  data-menu="tours">
                            <a href="?pages=tours">
                                <i class="ti-notepad"></i>
                                <p>Tours/Package</p>
                            </a>
                        </li>
                        <li data-menu="transfers">
                            <a href="?pages=transfers">
                                <i class="ti-notepad"></i>
                                <p>Transfers</p>
                            </a>
                        </li>
                        <li data-menu="customer">
                            <a href="?pages=customer">
                                <i class="ti-user"></i>
                                <p>Customer</p>
                            </a>
                        </li>
                        <li  data-menu="users">
                            <a href="?pages=users">
                                <i class="ti-comments-smiley"></i>
                                <p>Users</p>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>

            <div class="main-panel">
                <nav class="navbar navbar-default">
                    <div class="container-fluid">
                        <div class="navbar-header">
                            <button type="button" class="navbar-toggle">
                                <span class="sr-only">Toggle navigation</span>
                                <span class="icon-bar bar1"></span>
                                <span class="icon-bar bar2"></span>
                                <span class="icon-bar bar3"></span>
                            </button>
                            <a class="navbar-brand" href="#"><?= $_webname ?></a>
                        </div>
                        <div class="collapse navbar-collapse">
                            <ul class="nav navbar-nav navbar-right">
                                <li>
                                    <a href="?pages=settings">
                                        <i class="ti-settings"></i>
                                        <p>Settings</p>
                                    </a>
                                </li>
                                <li class="dropdown">
                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                        <i class="ti-user"></i>
                                        <p>
                                            <?php
                                            $user = select('user', "where id = $user_id", '');
                                            echo $user['name'];
                                            ?>
                                        </p>
                                        <b class="caret"></b>
                                    </a>
                                    <ul class="dropdown-menu">
                                        <li><a href="?pages=account">Account</a></li>
                                        <li><a href="logout.php">Log Out</a></li>
                                    </ul>
                                </li>
                            </ul>

                        </div>
                    </div>
                </nav>
                <?php
                switch ($page) {
                    case 'toursbooking':
                        include 'pages/toursbooking.php';
                        break;
                    case 'transfersbooking':
                        include 'pages/transfersbooking.php';
                        break;
                    case 'customer':
                        include 'pages/customer.php';
                        break;
                    case 'tours':
                        include 'pages/tours.php';
                        break;
                    case 'tourAdd':
                        include 'pages/tourAdd.php';
                        break;
                    case 'tourType':
                        include 'pages/tourType.php';
                        break;
                    case 'tourTypeAdd':
                        include 'pages/tourTypeAdd.php';
                        break;
                    case 'transfers':
                        include 'pages/transfers.php';
                        break;
                    case 'customer':
                        include 'pages/customer.php';
                        break;
                    case 'users':
                        include 'pages/users.php';
                        break;
                    case 'account':
                        include 'pages/account.php';
                        break;
                    case 'settings':
                        include 'pages/settings.php';
                        break;
                    default :
                        include 'pages/dashboard.php';
                        break;
                }
                ?>
                <footer class="footer">
                    <div class="container-fluid">
                        <nav class="pull-left">
                            <ul>
                                <li>
                                    <a href="http://www.phuketwebmodern.com">
                                        Phuket Web Modern | Support
                                    </a>
                                </li>
                            </ul>
                        </nav>
                        <div class="copyright pull-right">
                            &copy; <script>document.write(new Date().getFullYear())</script>, made with <i class="fa fa-heart heart"></i> by <a href="http://www.gotravelsystem.com">Go Travel System</a>
                        </div>
                    </div>
                </footer>
            </div>
        </div>
        <?php include_once 'inc/model.inc.php'; ?>
    </body>
    <!--   Core JS Files   -->
    <script src="assets/lib/jasny-bootstrap/js/jasny-bootstrap.min.js"></script>
    <script src="assets/lib/jquery-1.11.0.plugin.js"></script>
    <script src="assets/lib/datatable/js/jquery.dataTables.min.js"></script>
    <!-- Paper Dashboard DEMO methods, don't include it in your project! -->
    <script src="assets/js/demo.js"></script>
    <script src="assets/js/fn.js"></script>
    <script src="assets/js/app.js"></script>
    <script src="assets/lib/phuketwebmodern_function.js"></script>
</html>
