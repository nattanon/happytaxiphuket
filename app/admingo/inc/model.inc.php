<!-- Modal -->
<div class="modal fade" id="modelConfirm" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Confirm</h4>
            </div>
            <div class="modal-body">
                Are you sure you want to delete this item?
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">No</button>
                <button type="button" class="btn btn-primary" data-id="" data-btn="delete_yes">Yes</button>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="modelAddTourtype" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Add Tour Type</h4>
            </div>
            <div class="modal-body">
                <div class="content">
                    <form name='addtourtype' action="update.php">
                        <input type='hidden' name='id'>
                        <input type='hidden' name='page' value="tourType">
                        <div class="row">
                            <div class="col-md-8">
                                <div class="form-group">
                                    <label>Name</label>
                                    <input required type="text" class="form-control border-input" placeholder="" maxlength="100" name="name">
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for='tt'>Status</label>
                                    <select required class="form-control border-input" id="tt" name="status">
                                        <option value="1">Online</option>
                                        <option value="0">Not Online</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="text-center">
                            <button type="submit" class="btn btn-success">Save</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
