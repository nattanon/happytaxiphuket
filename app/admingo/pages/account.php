<?php
$_user = select('user', "where id = $user_id ", '');
?>
<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="header">
                        <h4 class="title">My Account</h4>
                    </div>
                    <div class="content">
                        <form action="update.php" method="POST">
                            <input type="hidden" name="page" value="account">
                            <input type="hidden" name="id" value="<?= $user_id ?>">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Full Name</label>
                                        <input value="<?= $_user['name'] ?>" name='name' required type="text" class="form-control border-input">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Position</label>
                                        <input value="<?= $_user['position'] ?>" disabled="" name='position' required type="text" class="form-control border-input" ">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Username</label>
                                        <input value="<?= $_user['username'] ?>" name='username' required type="text" class="form-control border-input" ">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Password</label>
                                        <input  value="<?= $_user['password'] ?>"  name='password' required type="password" class="form-control border-input" ">
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="text-center">
                                    <button type="submit" class="btn btn-success">Save</button>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                        </form>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
