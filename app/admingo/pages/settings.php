<?php
$conf = select('config', '', '');
?>
<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="header">
                        <h4 class="title">Settings</h4>
                    </div>
                    <div class="content">
                        <form action="set/updateSetting.php" method="POST">
                            <div class="title"> Website </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Web Name</label>
                                        <input value="<?= $conf['web_name'] ?>" name='web_name' required type="text" class="form-control border-input" ">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Address</label>
                                        <input value="<?= $conf['web_address'] ?>" name='web_address' required type="text" class="form-control border-input" ">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Path Logo</label>
                                        <input value="<?= $conf['web_logo'] ?>" name='web_logo' required type="text" class="form-control border-input" ">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Domain</label>
                                        <input  value="<?= $conf['web_domain'] ?>" placeholder="http://www.mywebsite.com/" name='web_domain' required type="text" class="form-control border-input" ">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Mobile Phone</label>
                                        <input value="<?= $conf['web_tel'] ?>" name='web_tel' required type="text" class="form-control border-input" ">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Email</label>
                                        <input value="<?= $conf['web_mail'] ?>" name='web_mail' required type="text" class="form-control border-input" ">
                                    </div>
                                </div>
                            </div>
                            <div class="title"> E-mail Setting </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label>E-mail Address (For send) </label>
                                        <input value="<?= $conf['email_mailAddress'] ?>" placeholder="abc@mywebsite.com" name='email_mailAddress' required type="text" class="form-control border-input" ">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Username (For login)</label>
                                        <input value="<?= $conf['email_username'] ?>" placeholder="abc@mywebsite.com" name='email_username' required type="text" class="form-control border-input" ">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Password</label>
                                        <input value="<?= $conf['email_password'] ?>" placeholder="12345678" name='email_password' required type="text" class="form-control border-input" ">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Host</label>
                                        <input value="<?= $conf['email_host'] ?>" placeholder="smtp.mywebsite.com" name='email_host' required type="text" class="form-control border-input" ">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Port</label>
                                        <input value="<?= $conf['email_port'] ?>" placeholder="448" name='email_port' required type="text" class="form-control border-input" ">
                                    </div>
                                </div>
                            </div>
                            <div class="title"> Paypal Setting </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Paypal Account</label>
                                        <input value="<?= $conf['paypal_account'] ?>" name='paypal_account'   type="text" class="form-control border-input" ">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="text-center">
                                    <button type="submit" class="btn btn-success">Save</button>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                        </form>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
