<?php
error_reporting(E_ALL ^ E_NOTICE);
$ae = "Add";
//isset($_REQUEST['id']) ? $id = $_REQUEST['id'] : $id = '';
if (isset($_REQUEST['id'])) {
    $tour_id = $_REQUEST['id'];
    $ae = "Edit";
    $tour = select('tour', "where id = $tour_id", '');
} else {
    $tour_id = '';
}
?>
<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="header">
                        <h4 class="title"><?= $ae ?> Tour</h4>
                    </div>
                    <div class="content">
                        <form action="set/addtour.php" method="POST" enctype="multipart/form-data">
                            <input type='hidden'  name='id' value="<?= $tour['id'] ?>"> 
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Tour Name</label>
                                        <input name='name' required type="text" class="form-control border-input" placeholder="Phi Phi Island Tours" value="<?= $tour['name'] ?>">
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label for="tt">Tour Type</label>
                                        <select required class="form-control border-input" id="tt" name="tour_type">
                                            <?php
                                            $ttype = selects('tour_type', 'where status=1 order by name asc', '');
                                            echo "<option disabled selected>-- Select --</option>";
                                            foreach ($ttype as $value) {
                                                ($value['id'] == $tour['tour_type']) ? $select = "selected" : $select = '';
                                                ?>
                                                <option <?= $select ?> value="<?= $value['id'] ?>"><?= $value['name'] ?></option>
                                                <?php
                                            }
                                            ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label for="tc">Tour Catagory</label>
                                        <select class="form-control border-input" id="tc" name="tour_cat" sys_pro>

                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label>Home Tour Information</label>
                                        <input required type="text" class="form-control border-input" placeholder="" maxlength="100" name="homedec" value="<?= $tour['homedec'] ?>">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label>Full Tour Information</label>
                                        <textarea required class="form-control border-input" rows="5" id="ckeditor" name="fulldes"><?= $tour['fulldes'] ?></textarea>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Itinerary</label>
                                        <textarea class="form-control border-input" rows="5" id="itinerary" name="itinerary" ><?= $tour['itinerary'] ?></textarea>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Tour Include</label>
                                        <textarea class="form-control border-input" rows="5" id="tourinclude" name="tourinclude"><?= $tour['tourinclude'] ?></textarea>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>What To Bring </label>
                                        <textarea class="form-control border-input" rows="5" id="whattobing" name="whattobing"><?= $tour['whattobting'] ?></textarea>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Transfers Fee</label>
                                        <textarea class="form-control border-input" rows="5" id="transferfee" name="transferfee"><?= $tour['transfersfee'] ?></textarea>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label>Price Adult</label>
                                        <input required type="number" class="form-control border-input" placeholder="1500" name="adult_net_price" id="adult_net_price" value="<?= $tour['adult_net_price'] ?>">
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="child_net_price">Price Child</label>
                                        <input required type="number" class="form-control border-input" placeholder="1200" name="child_net_price" id="child_net_price" value="<?= $tour['child_net_price'] ?>">
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label>Price Promotion</label>
                                        <select class="form-control border-input" id="promotion" name="promotion" sys_pro >
                                            <option <?php
                                            if ($tour['promotion'] == 0) {
                                                echo "selected";
                                            }
                                            ?> value="0">Not have Promotion</option>
                                            <option <?php
                                            if ($tour['promotion'] == 1) {
                                                echo "selected";
                                            }
                                            ?> value="1">Have Promotion</option>
                                        </select>
                                    </div>
                                </div>
                            </div>

                            <div class="row" hidden="">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Price Adult Promotion</label>
                                        <input type="number" class="form-control border-input" placeholder="1500" name="adult_pro_price" id="adult_pro_price" value="<?= $tour['adult_pro_price'] ?>">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="child_net_price">Price Child Promotion</label>
                                        <input type="number" class="form-control border-input" placeholder="1200" name="child_pro_price" id="child_pro_price" value="<?= $tour['child_pro_price'] ?>">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label> Image Home </label>
                                        <div class="fileinput fileinput-new input-group" data-provides="fileinput">
                                            <div class="form-control" data-trigger="fileinput"><i class="glyphicon glyphicon-file fileinput-exists"></i> <span class="fileinput-filename"></span></div>
                                            <span class="input-group-addon btn btn-default btn-file"><span class="fileinput-new">Select file</span><span class="fileinput-exists">Change</span><input type="file" name="img_home"></span>
                                            <a href="#" class="input-group-addon btn btn-default fileinput-exists" data-dismiss="fileinput">Remove</a>
                                        </div>
                                    </div>
                                    <?php
                                    if ($tour['img_home']) {
                                        echo "<img class='center' style='width: 100%;' src='../../images/tours/" . $tour['img_home'] . "'>";
                                    }
                                    ?>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label> Image 1 </label>
                                        <div class="fileinput fileinput-new" data-provides="fileinput">
                                            <span class="btn btn-default btn-file"><span class="fileinput-new">Select file</span><span class="fileinput-exists">Change</span><input type="file" name="img_1"></span>
                                            <span class="fileinput-filename"></span>
                                            <a href="#" class="close fileinput-exists" data-dismiss="fileinput" style="float: none">&times;</a>
                                        </div>
                                        <?php
                                        if ($tour['img_1']) {
                                            echo "<img style='width: 100%;' src='../../images/tours/" . $tour['img_1'] . "'>";
                                        }
                                        ?>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label>Image 2</label>
                                        <div class="fileinput fileinput-new" data-provides="fileinput">
                                            <span class="btn btn-default btn-file"><span class="fileinput-new">Select file</span><span class="fileinput-exists">Change</span><input type="file" name="img_2"></span>
                                            <span class="fileinput-filename"></span>
                                            <a href="#" class="close fileinput-exists" data-dismiss="fileinput" style="float: none">&times;</a>
                                        </div>
                                        <?php
                                        if ($tour['img_2']) {
                                            echo "<img style='width: 100%;' src='../../images/tours/" . $tour['img_2'] . "'>";
                                        }
                                        ?>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label>Image 3</label>
                                        <div class="fileinput fileinput-new" data-provides="fileinput">
                                            <span class="btn btn-default btn-file"><span class="fileinput-new">Select file</span><span class="fileinput-exists">Change</span><input type="file" name="img_3"></span>
                                            <span class="fileinput-filename"></span>
                                            <a href="#" class="close fileinput-exists" data-dismiss="fileinput" style="float: none">&times;</a>
                                        </div>
                                        <?php
                                        if ($tour['img_3']) {
                                            echo "<img style='width: 100%;' src='../../images/tours/" . $tour['img_3'] . "'>";
                                        }
                                        ?>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label>Image 4</label>
                                        <div class="fileinput fileinput-new" data-provides="fileinput">
                                            <span class="btn btn-default btn-file"><span class="fileinput-new">Select file</span><span class="fileinput-exists">Change</span><input type="file" name="img_4"></span>
                                            <span class="fileinput-filename"></span>
                                            <a href="#" class="close fileinput-exists" data-dismiss="fileinput" style="float: none">&times;</a>
                                        </div>
                                    </div>
                                    <?php
                                    if ($tour['img_4']) {
                                        echo "<img style='width: 100%;' src='../../images/tours/" . $tour['img_4'] . "'>";
                                    }
                                    ?>
                                </div>

                            </div>
                            <div class="row">
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label> </label>
                                        <div class="checkbox border-input">
                                            <label><input type="checkbox" class="" name="slider" value="1" <?php
                                                if ($tour['slider'] == 1) {
                                                    echo "checked";
                                                }
                                                ?> >Slider</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label> </label>
                                        <div class="checkbox border-input">
                                            <label><input type="checkbox" class="" name="onlinestatus" value="1" sys_pro <?php
                                                if ($tour['onlinestatus'] == 1) {
                                                    echo "checked";
                                                }
                                                ?>>Online</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label> </label>
                                        <div class="checkbox border-input">
                                            <label><input type="checkbox" class="" name="tour_noplus" value="1" sys_pro <?php
                                                if ($tour['tour_noplus'] == 1) {
                                                    echo "checked";
                                                }
                                                ?>>Tour Per Vehicle</label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="text-center">
                                <button type="submit" class="btn btn-success"><?= $ae ?></button>
                            </div>
                            <div class="clearfix"></div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    $(function () {
        CKEDITOR.replace('ckeditor', {
            language: 'en',
            uiColor: '#CCC5B9'
        });
    });
</script>