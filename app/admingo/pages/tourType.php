<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="text-right" style="padding: 10px">
                        <button class="btn btn-basic"  data-btn='tourTypeAdd'> + Add Type </button>
                    </div>
                    <div class="header">
                        <h4 class="title">Tour Type</h4> 
                    </div>
                    <div class="content">
                        <div class="content table-responsive table-full-width">
                            <table class="table  table-hover dataTable">
                                <thead>
                                <th>#</th>
                                <th>Name</th>
                                <th>Status</th>
                                <th>Setting</th>
                                </thead>
                                <tbody>
                                    <?php
                                    $i = 0;
                                    $ttype = selects('tour_type', 'order by name asc', 'id,name,status');
                                    foreach ($ttype as $val) {
                                        $i ++;
                                        ?>
                                        <tr>
                                            <td><?= $i ?> </td>
                                            <td><?= $val['name'] ?> </td>
                                            <td><?= fnonline_status($val['status']) ?> </td>
                                            <td><button type="button" data-btn='tourTypeAdd' data-id="<?= $val['id'] ?>" class="btn btn-info btn-sm">Edit</button>&nbsp;<button type="button" data-btn="delete" data-id="<?= $val['id'] ?>"  class="btn btn-danger btn-sm">Delete</button></td>
                                        </tr>
                                        <?php
                                    }
                                    ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

