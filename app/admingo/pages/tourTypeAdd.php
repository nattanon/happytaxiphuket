<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="header">
                        <h4 class="title">Add Tour Type</h4>
                    </div>
                    <div class="content">
                        <form name='addtourtype'>
                            <div class="row">
                                <div class="col-md-8">
                                    <div class="form-group">
                                        <label>Name</label>
                                        <input required type="text" class="form-control border-input" placeholder="" maxlength="100" name="name">
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for='tt'>Status</label>
                                        <select required class="form-control border-input" id="tt" name="status">
                                            <option value="1">Online</option>
                                            <option value="0">Not Online</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="text-center">
                                <button type="submit" class="btn btn-success">Save</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

