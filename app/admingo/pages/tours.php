<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="text-right" style="padding: 10px">
                        <button class="btn btn-basic" data-btn="tourtype"> + Add Type </button>
                        <button class="btn btn-warning" data-btn="addpackage"> + Add Tour/Package </button>
                    </div>
                    <div class="header">
                        <h4 class="title">All Tours / Package</h4>
                        <!--<p class="category">Last 10 Items</p>-->
                    </div>
                    <div class="content">
                        <div class="content table-responsive table-full-width">
                            <table class="table  table-hover dataTable">
                                <thead>
                                <th>ID</th>
                                <th>Name</th>
                                <th>Adult</th>
                                <th>Child</th>
                                <th>Setting</th>
                                </thead>
                                <tbody>
                                    <?php
                                    $i = 0;
                                    $tours = selects('tour', '', 'id,adult_net_price,child_net_price,name');
                                    foreach ($tours as $val) {
                                        $i ++;
                                        ?>
                                        <tr>
                                            <td><?= $i ?> </td>
                                            <td><?= $val['name'] ?> </td>
                                            <td><?= number_format($val['adult_net_price']) ?> </td>
                                            <td><?= number_format($val['child_net_price']) ?> </td>
                                            <td><button data-btn='tourAdd' data-id="<?= $val['id'] ?>"  type="button" class="btn btn-info btn-sm">Edit</button>&nbsp;<button type="button" data-btn="delete" data-id="<?= $val['id'] ?>" class="btn btn-danger btn-sm">Delete</button></td>
                                        </tr>
                                        <?php
                                    }
                                    ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>