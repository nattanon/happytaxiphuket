<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">
<div style="padding:6px;">
    <?php
    include 'inc/config.php';
    include 'inc/function.php';
    $r = selects('news', 'where status = 1', 'id,topic,description');
    $i = 0;
    foreach ($r as $v) {
        $i++;
        ?>
        <div class="panel panel-info">
            <div class="panel-heading"><?= $v['topic'] ?></div>
            <div class="panel-body"><?= $v['description'] ?></div>
        </div>
        <?php
    }
    ?>
</div>