<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">
<div style="padding:6px;">
    <?php
    $i = 0;
    $xml = simplexml_load_file("http://www.phuketwebmodern.com/news/xml.php") or die("Cannot have object");
    foreach ($xml->children() as $news) {
        $i++;
        ?>
        <div class="panel panel-info">
            <div class="panel-heading"><?= $i . '. ' . $news->topic ?></div>
            <div class="panel-body"><?= $news->description ?></div>
        </div>
        <?php
    }
    ?>
</div>