<?php

session_start();
$user_id = $_SESSION['UserID'];
empty($_POST) ? $name = '' : $name = $_POST['name'];
empty($_POST) ? $id = '' : $id = $_POST['id'];
if ($name != '') {
    include '../../../conf/config.php';
    include '../../../conf/function.php';
    $target_dir = "../../../images/tours/";
    $expensions = array("jpeg", "jpg", "png");
    $filesize = 2097152;

    isset($_REQUEST['tour_type']) ? $tour_type = $_REQUEST['tour_type'] : $tour_type = '';
    isset($_REQUEST['tour_cat']) ? $tour_cat = $_REQUEST['tour_cat'] : $tour_cat = '';
    isset($_REQUEST['homedec']) ? $homedec = $_REQUEST['homedec'] : $homedec = '';
    isset($_REQUEST['fulldes']) ? $fulldes = $_REQUEST['fulldes'] : $fulldes = '';
    isset($_REQUEST['itinerary']) ? $itinerary = $_REQUEST['itinerary'] : $itinerary = '';
    isset($_REQUEST['tourinclude']) ? $tourinclude = $_REQUEST['tourinclude'] : $tourinclude = '';
    isset($_REQUEST['whattobing']) ? $whattobing = $_REQUEST['whattobing'] : $whattobing = '';
    isset($_REQUEST['transferfee']) ? $transferfee = $_REQUEST['transferfee'] : $transferfee = '';
    isset($_REQUEST['adult_net_price']) ? $adult_net_price = $_REQUEST['adult_net_price'] : $adult_net_price = 0;
    isset($_REQUEST['adult_net_price']) ? $child_net_price = $_REQUEST['child_net_price'] : $child_net_price = 0;
    isset($_REQUEST['promotion']) ? $promotion = $_REQUEST['promotion'] : $promotion = 0;
    isset($_REQUEST['adult_pro_price']) ? $adult_pro_price = $_REQUEST['adult_pro_price'] : $adult_pro_price = 0;
    isset($_REQUEST['child_pro_price']) ? $child_pro_price = $_REQUEST['child_pro_price'] : $child_pro_price = 0;
    isset($_REQUEST['slider']) ? $slider = $_REQUEST['slider'] : $slider = 0;
    isset($_REQUEST['onlinestatus']) ? $onlinestatus = $_REQUEST['onlinestatus'] : $onlinestatus = 0;
    isset($_REQUEST['tour_noplus']) ? $tour_noplus = $_REQUEST['tour_noplus'] : $tour_noplus = 0;

    $img_home = '';
    $img_1 = '';
    $img_2 = '';
    $img_3 = '';
    $img_4 = '';
    
    
    $vowels = array('(',')','<','>');
    $namer = str_replace($vowels,'',replace_blank($name));

    if (isset($_FILES['img_home']) && $_FILES['img_home']['size'] > 0) {
        $filename = $_FILES['img_home']['name'];
        $file_size = $_FILES['img_home']['size'];
        $file_ext = pathinfo($filename, PATHINFO_EXTENSION);

        if ($file_size > $filesize) {
            $errors[] = 'img_home File size must be excately 2 MB';
        }
        if (in_array($file_ext, $expensions) === false) {
            $errors[] = "img_home extension not allowed, please choose a JPEG or PNG file.";
        }
        if (empty($errors) == true) {
            $temp = explode(".", $filename);
            $img_home = round(microtime(true)) . '-' . $namer . '-1.' . end($temp);
            move_uploaded_file($_FILES["img_home"]["tmp_name"], $target_dir . $img_home);
        }
    }

    if (isset($_FILES['img_1']) && $_FILES['img_1']['size'] > 0) {
        $filename = $_FILES['img_1']['name'];
        $file_size = $_FILES['img_1']['size'];
        $file_ext = pathinfo($filename, PATHINFO_EXTENSION);
        if ($file_size > $filesize) {
            $errors[] = 'img_1 File size must be excately 2 MB';
        }
        if (in_array($file_ext, $expensions) === false) {
            $errors[] = "img_1 extension not allowed, please choose a JPEG or PNG file.";
        }
        if (empty($errors) == true) {
            $temp = explode(".", $_FILES["img_1"]["name"]);
            $img_1 = round(microtime(true)) . '-' . $namer . '-2.' . end($temp);
            move_uploaded_file($_FILES["img_1"]["tmp_name"], $target_dir . $img_1);
        }
    }
    if (isset($_FILES['img_2']) && $_FILES['img_2']['size'] > 0) {
        $filename = $_FILES['img_2']['name'];
        $file_size = $_FILES['img_2']['size'];
        $file_ext = pathinfo($filename, PATHINFO_EXTENSION);
        if ($file_size > $filesize) {
            $errors[] = ' img_2 File size must be excately 2 MB';
        }
        if (in_array($file_ext, $expensions) === false) {
            $errors[] = "img_2 extension not allowed, please choose a JPEG or PNG file.";
        }
        if (empty($errors) == true) {
            $temp = explode(".", $_FILES["img_2"]["name"]);
            $img_2 = round(microtime(true)) . '-' . $namer . '-3.' . end($temp);
            move_uploaded_file($_FILES["img_2"]["tmp_name"], $target_dir . $img_2);
        }
    }
    if (isset($_FILES['img_3']) && $_FILES['img_3']['size'] > 0) {
        $filename = $_FILES['img_3']['name'];
        $file_size = $_FILES['img_3']['size'];
        $file_ext = pathinfo($filename, PATHINFO_EXTENSION);
        if ($file_size > $filesize) {
            $errors[] = 'img_3 File size must be excately 2 MB';
        }
        if (in_array($file_ext, $expensions) === false) {
            $errors[] = "img_3 extension not allowed, please choose a JPEG or PNG file.";
        }
        if (empty($errors) == true) {
            $temp = explode(".", $_FILES["img_3"]["name"]);
            $img_3 = round(microtime(true)) . '-' . $namer . '-4.' . end($temp);
            move_uploaded_file($_FILES["img_3"]["tmp_name"], $target_dir . $img_3);
        }
    }
    if (isset($_FILES['img_4']) && $_FILES['img_4']['size'] > 0) {
        $filename = $_FILES['img_4']['name'];
        $file_size = $_FILES['img_4']['size'];
        $file_ext = pathinfo($filename, PATHINFO_EXTENSION);
        if ($file_size > $filesize) {
            $errors[] = 'img_4 File size must be excately 2 MB';
        }
        if (in_array($file_ext, $expensions) === false) {
            $errors[] = "img_4 extension not allowed, please choose a JPEG or PNG file.";
        }
        if (empty($errors) == true) {
            $temp = explode(".", $_FILES["img_4"]["name"]);
            $img_4 = round(microtime(true)) . '-' . $namer . '-5.' . end($temp);
            move_uploaded_file($_FILES["img_4"]["tmp_name"], $target_dir . $img_4);
        }
    }
    if (!empty($errors)) {
        foreach ($errors as $key => $value) {
            $er = $value;
            echo "<script>alert('$er')</script>";
        }
        echo "<script>history.back();</script>";
        exit();
    }

    if ($id) {
        $sql = "UPDATE tour set "
                . "adult_net_price = '$adult_net_price', adult_pro_price = '$adult_pro_price', "
                . "child_net_price = '$child_net_price', child_pro_price = '$child_pro_price',"
                . "fulldes = '$fulldes',homedec = '$homedec',itinerary = '$itinerary',name = '$name',"
                . "onlinestatus = $onlinestatus, price_ac = 0, promotion = $promotion,"
                . "slider = $slider, tourinclude = '$tourinclude', "
                . "tour_noplus = $tour_noplus, tour_type = $tour_type, transfersfee = '$transferfee', "
                . "whattobting = '$whattobing' ";

        if ($img_home) {
            $sql .= " ,img_home = '$img_home' ";
        }
        if ($img_1) {
            $sql .= " ,img_1 = '$img_1' ";
        }
        if ($img_2) {
            $sql .= " ,img_2 = '$img_2' ";
        }
        if ($img_3) {
            $sql .= " ,img_3 = '$img_3' ";
        }
        if ($img_4) {
            $sql .= " ,img_4 = '$img_4' ";
        }
        $sql .= " where id = $id";
    } else {
        $sql = "insert into tour "
                . "(adult_net_price,adult_pro_price,child_net_price,child_pro_price,"
                . "fulldes,homedec,img_1,img_2,img_3,img_4,img_home,itinerary,name,"
                . "onlinestatus,price_ac,promotion,slider,tourinclude,"
                . "tour_noplus,tour_type,transfersfee,user_id,whattobting) values ("
                . "$adult_net_price,'$adult_pro_price',$child_net_price,'$child_pro_price',"
                . "'$fulldes','$homedec','$img_1','$img_2','$img_3','$img_4','$img_home','$itinerary','$name',"
                . "$onlinestatus,0,$promotion,$slider,'$tourinclude',"
                . "$tour_noplus,$tour_type,'$transferfee',$user_id,'$whattobing');";
    }

    if (mysql_query($sql)) {
//        $id = mysql_insert_id();
        echo "<script>alert('บันทึกสำเร็จ');</script>";
        echo "<script>window.location = '../dashboard.php?pages=tours';</script>";
    } else {
        echo "<script>alert(ผิดพลาด!!!!');</script>";
    }
} else {
    echo "<script>alert('NO!!!!');</script>";
}
?>