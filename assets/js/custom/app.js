$(function () {
    //Menu 
    $.simpleWeather({
        location: 'Phuket, TH',
        woeid: '',
        unit: 'c',
        success: function (weather) {
            html = '<h2><i class="icon-' + weather.code + '"></i> ' + weather.temp + '&deg;' + weather.units.temp + '</h2>';
            html += '<p>' + weather.currently + '</p>';
            html += '<p>' + weather.city + ', ' + weather.region + '</p>';

            $("#weather").html(html);
        },
        error: function (error) {
            // alert('er')
            console.log("Not connect get the weather");
            $("#weather").html('<p>' + error + '</p>');
        }
    });

    $('.image-popup-no-margins').magnificPopup({
        type: 'image',
        closeOnContentClick: true,
        closeBtnInside: false,
        fixedContentPos: true,
        mainClass: 'mfp-no-margins mfp-with-zoom', // class to remove default margin from left and right side
        image: {
            verticalFit: true
        },
        zoom: {
            enabled: true,
            duration: 300 // don't foget to change the duration also in CSS
        }
    });

    $('[data-role="datetime"]').datetimepicker({
        inline: false,
        format: 'Y-m-d H:i',
        zIndex: 9999
    });
    $('[data-link]').click(function () {
        var link = $(this).data('link')
        window.location = link;
    });
    $('[data-toggle="popover"]').popover();
    $('[data-toggle="tooltip"]').tooltip();

    $('form[name="booking"]').submit(function () {
        var $self = $(this);
        $.isLoading({
            text: "Booking",
            position: "overlay",
            'class': "icon-refresh",
        });
        setTimeout(function () {
            $.ajax({
                type: 'POST',
                url: 'addBook.php',
                dataType: 'html',
                data: $self.serialize(),
                success: function (r) {
                      
                    if (r === 'invalidcaptcha') {
                        alert('Invalid captcha');
                        $('#change-image').trigger('click');
                        $.isLoading("hide");
                        return;
                    } else if (r === 'spam') {
                        alert('You are spammer ! Get the @$%K out');
                        return;
                    }
                    if (r !== 'error') {
                        alert('Booking Done!');
                        $.isLoading("hide");
                        $.isLoading({text: "Booking Done! Please Wait "});
                        setTimeout(function () {
                            window.location = '.';
//                            window.location = 'payment.php?id=' + r.trim() + '&tk=' + makeid() + '';
                        }, 1500);
                    }
                }, error: function (jqXHR, textStatus, errorThrown) {
                    console.log(jqXHR.responseText);
                    console.log(textStatus, errorThrown);
                    alert('error please contact admin');
                }});
        }, 1500);
    });
    $('[name="contact_form"]').submit(function () {
        var $self = $(this);
        $.isLoading({
            text: "Waiting",
            position: "overlay",
            'class': "icon-refresh",
        });
        setTimeout(function () {
            $.ajax({
                type: 'POST',
                url: 'sentemail.php',
                dataType: 'html',
                data: $self.serialize(),
                success: function (r) {
                    console.log(r);
                    r = $.trim(r);
                    if (r === '0') {
                        alert('Error');
                        $.isLoading("hide");
                        return;
                    } else if (r === '1') {
                        alert('Sent Done');
                        $.isLoading("hide");
                        return;
                    }
                }, error: function (jqXHR, textStatus, errorThrown) {
                    console.log(jqXHR.responseText);
                    console.log(textStatus, errorThrown);
                    alert('error please contact admin');
                }});
        }, 1500);
    });
});