<?php

include_once 'config.php';
include_once 'function.php';
$_dt = select('config', '', 'web_name,web_tel,web_domain,web_address,web_mail,web_logo,paypal_account,email_host,email_mailAddress,email_username,email_password,email_port,sysversion');
$_webname = $_dt[0];
$_webtel = $_dt[1];
$_domain = $_dt[2];
$_webaddress = $_dt[3];
$_webmail = $_dt[4];
$_weblogo = $_dt[5];
$_title = $_webname;

$_paypalaccount = $_dt[6];

$_emailhost = $_dt[7];
$_emailAddress = $_dt[8];
$_emailUsername = $_dt[9];
$_emailPassword = $_dt[10];
$_emailPort = $_dt['email_port'];

$version = "?version=1";

$_admin_sys_version = (int) $_dt[12];
if ($_admin_sys_version == 0) {
    $_txt_get_full_version = ' <div class="card text-center red"> This Option for Full Version Only <button type="button" data-btn="getfullversion" class="btn bg-primary">Get Full Version?</button> </div>';
}
?>