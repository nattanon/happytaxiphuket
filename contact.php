<?php session_start() ?>
<!--DB-->
<?php
$_title2 = 'Contact us';
include 'conf/getGeneral.php';
?>
<!DOCTYPE html>
<html>
    <?php include 'inc/head.inc.php'; ?>
    <body>
        <?php include 'pages/header.php' ?>
        <aside id="fh5co-hero" class="js-fullheight">
            <div class="flexslider js-fullheight">
                <ul class="slides">
                <?php 
                    $image_t = selects('tour', "where slider = 1 ORDER BY RAND() LIMIT 1", '');
                ?>
                <li style="background-image: url(images/tours/<?= $image_t[0]['img_home'] ?>);">
                    <div class="overlay-gradient"></div>
                    <div class="container">
                        <div class="col-md-10 col-md-offset-1 text-center js-fullheight slider-text">
                            <div class="slider-text-inner desc">
                                <h2 class="heading-section">Contact Information</h2>
                                <p class="fh5co-lead">Service is friendly. <span style="color:Yellow;">Happy Taxi Phuket</span></p>
                            </div>
                        </div>
                    </div>
                </li>
                </ul>
            </div>
        </aside>

        
        <div id="fh5co-content">
            
            <div class="choose animate-box">
                <div class="fh5co-heading">
                    <h2>Contact Information</h2>
                    <ul class="fh5co-footer-links">
						<li>35/9 Baan Chao Fah nougat Garden Home 5 (beside Thalang) Moo 3 Tambol vichit, Amphur muang, Phuket 83000, Thailand</li>
						<li><a href="mailto:info@happytaxiphuket.com">info@happytaxiphuket.com</a></li>
                        <li><a href="tel://0950197638">Tel. 0950197638</a></li>
						<li><a href="tel://0896529296">Tel. 0896529296</a></li>
						<li><a href="Wechat://dl/chat?app0950197638">Wechat ID: app0950197638</a></li>
						<li><a href="https://api.whatsapp.com/send?phone=0950197638">Whatsapp ID: 0950197638</a></li>
                    </ul>       
                    <div>
                    <img style="  
	height: 120px; 
	background: #0E9F98;
	float: left;" src="images/w1.jpg" alt="Can not find image Happy Taxi Phuket" class="img-responsive ismax">
                    <img style=" 
	height: 120px;
	float: right;"src="images/w2.jpg" alt="Can not find image Happy Taxi Phuket" class="img-responsive ismax">

                    </div>
                </div>
            </div>
            <div class="video fh5co-video" >
            <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3952.26026067751!2d98.34658031477858!3d7.867810994331419!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x0!2zN8KwNTInMDQuMSJOIDk4wrAyMCc1NS42IkU!5e0!3m2!1sen!2sth!4v1540203047032" width="100%" height="100%" ></iframe>
            </div>
        </div>

        
        
        <div id="fh5co-project">
            <div class="container fix">
                <div class="row animate-box">
                    <div class="col-md-8 col-md-offset-2 text-center fh5co-heading">
					    <h2>Popular Tours</h2>
                        <p>"Tours are popular and very impressive. We have a choice.</p>
                    </div>
                </div>
            </div>
            <div class="container">
                <div class="row">
                    <?php
                        $tour_r1 = selects('tour', "WHERE slider = 1 ORDER BY RAND() LIMIT 6", '');
                        $ti = 0;
                        foreach ($tour_r1 as $tr) {
                            $url = "tour-" . $tr['id'] . "-" . replace_blank($tr['name']) . "";
                            $ti++;
                    ?>
                        <div class="col-md-4 col-sm-6 text-center fh5co-project animate-box" data-animate-effect="fadeIn">
                            <a href="<?= $url ?>" >
                                <img src="images/tours/<?= $tr['img_home'] ?>" alt="Can not find image Happy Taxi Phuket" class="img-responsive ismax">  
                                <span><?= $tr['name'] ?></span>
                            </a>
                        </div>
                    <?php } ?>
                </div>
            </div>
        </div>

        <?php include 'pages/footer.php'; ?>
        <?php include 'inc/jsfoot.inc.php'; ?>
    </body>
</html>