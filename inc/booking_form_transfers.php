<?php
if (isset($tour['id'])) {
    $tourid = $tour['id'];
}else{
    $tourid = 9999;
}
?>
<div class="book_form">
    <form onsubmit="return false" name="booking" role="form" id="booking_form">
        <input type="hidden" name="tour_id" value="<?=$tourid?>">
        <div class="divider"></div>
        <h4>Booking Form</h4>
        <div class="divider"></div>
        <div class="row">
            <div class='col-sm-6'>    
                <div class='form-group'>
                    <label for="firstname">First Name <span class="red">*</span></label>
                    <input class="form-control" type="text" required name="firstname" id="firstname">
                </div>
            </div>

            <div class='col-sm-6'>    
                <div class='form-group'>
                    <label for="lastname">Last Name <span class="red">*</span></label>
                    <input class="form-control" type="text" required name="lastname" id="lastname">
                </div>
            </div>

            <div class='col-sm-6'>    
                <div class='form-group'>
                    <label for="ct">Country Codes<span class="red">*</span></label>
                    <input class="form-control" type="text" required name="ct" id="ct" placeholder="+66">
                     <!-- <select class="form-control" id="ct" required name="ct">
                    </select> -->
                </div>
            </div>

            <div class='col-sm-6'>    
                <div class='form-group'>
                    <label for="phone">Phone <span class="red">*</span></label>
                    <input class="form-control" type="text" required name="phone" id="phone">
                </div>
            </div>

            <div class='col-sm-6'>    
                <div class='form-group'>
                    <label for="email">Email <span class="red">*</span></label>
                    <input class="form-control" type="text" required name="email" id="email" placeholder="customer@email.com">
                </div>
            </div>

            <div class='col-sm-6'>    
                <div class='form-group'>
                    <label for="nationality">Nationality <span class="red">*</span></label>
                    <select class="form-control" id="nationality" required name="nationality">
                        <option value="">Select Nationality</option>
                        <option>Albanian</option>
                        <option>Algerian</option>
                        <option>American</option>
                        <option>Antiguan</option>
                        <option>Argentine</option>
                        <option>Armenian</option>
                        <option>Australian</option>
                        <option>Austrian</option>
                        <option>Bahamian</option>
                        <option>Bahraini</option>
                        <option>Bangladeshi</option>
                        <option>Barbadian</option>
                        <option>Belarusian</option>
                        <option>Belgian</option>
                        <option>Belizean</option>
                        <option>Beninese</option>
                        <option>Bermudian</option>
                        <option>Bhutanese</option>
                        <option>Bolivian</option>
                        <option>Brazilian</option>
                        <option>British</option>
                        <option>Bruneian</option>
                        <option>Bulgarian</option>
                        <option>Burmese</option>
                        <option>Cambodian</option>
                        <option>Cameroonian</option>
                        <option>Canadian</option>
                        <option>Caymanian</option>
                        <option>Chadian</option>
                        <option>Chilean</option>
                        <option>Chinese</option>
                        <option>Chinese (Hong Kong)</option>
                        <option>Colombian</option>
                        <option>Comoran</option>
                        <option>Costa Rican</option>
                        <option>Croatian</option>
                        <option>Cuban</option>
                        <option>Cypriot</option>
                        <option>Czech</option>
                        <option>Danish</option>
                        <option>Dominican</option>
                        <option>Dutch</option>
                        <option>Ecuadorian</option>
                        <option>Egyptian</option>
                        <option>Emirati</option>
                        <option>Equatoguinean</option>
                        <option>Estonian</option>
                        <option>Falkland Island</option>
                        <option>Fijian</option>
                        <option>Finnish</option>
                        <option>French </option>
                        <option>French Polynesian</option>
                        <option>Gabonese</option>
                        <option>Gambian</option>
                        <option>Georgian</option>
                        <option>German</option>
                        <option>Ghanaian</option>
                        <option>Gibraltar</option>
                        <option>Greek</option>
                        <option>Greenlandic</option>
                        <option>Grenadian</option>
                        <option>Guadeloupe</option>
                        <option>Guamanian</option>
                        <option>Guatemalan</option>
                        <option>Guinean</option>
                        <option>Guinean</option>
                        <option>Guyanese</option>
                        <option>Honduran</option>
                        <option>Hong Kong</option>
                        <option>Hungarian</option>
                        <option>Icelandic</option>
                        <option>Indian</option>
                        <option>Indonesian</option>
                        <option>Iranian</option>
                        <option>Iraqi</option>
                        <option>Irish</option>
                        <option>Israeli</option>
                        <option>Italian</option>
                        <option>Jamaican</option>
                        <option>Japanese</option>
                        <option>Jordanian</option>
                        <option>Kazakhstani</option>
                        <option>Kenyan</option>
                        <option>Korean (North)</option>
                        <option>Korean (South)</option>
                        <option>Kuwaiti</option>
                        <option>Laotian</option>
                        <option>Latvian</option>
                        <option>Lebanese</option>
                        <option>Liberian</option>
                        <option>Libyan</option>
                        <option>Liechtenstein</option>
                        <option>Lithuania </option>
                        <option>Luxembourg</option>
                        <option>Macedonian</option>
                        <option>Malagasy</option>
                        <option>Malawian</option>
                        <option>Malaysian</option>
                        <option>Maldivian</option>
                        <option>Malian</option>
                        <option>Maltese</option>
                        <option>Mauritian</option>
                        <option>Mexican</option>
                        <option>Moldovan</option>
                        <option>Mongolian</option>
                        <option>Montenegrin</option>
                        <option>Montserratian</option>
                        <option>Moroccan</option>
                        <option>Motswana</option>
                        <option>Mozambican</option>
                        <option>Namibian</option>
                        <option>Nepalese</option>
                        <option>New Caledonian</option>
                        <option>New Zealand</option>
                        <option>Nicaraguan</option>
                        <option>Nigerian</option>
                        <option>Nigerien</option>
                        <option>Norwegian</option>
                        <option>Omani</option>
                        <option>Pakistani</option>
                        <option>Palauan</option>
                        <option>Panamanian</option>
                        <option>Papua New Guinean</option>
                        <option>Paraguayan</option>
                        <option>Peruvian</option>
                        <option>Philippine</option>
                        <option>Polish</option>
                        <option>Portuguese</option>
                        <option>Puerto Rican</option>
                        <option>Qatari</option>
                        <option>Romanian</option>
                        <option>Russian</option>
                        <option>Rwandan</option>
                        <option>Salvadoran</option>
                        <option>Sammarinese</option>
                        <option>Samoan</option>
                        <option>Saudi Arabian</option>
                        <option>Senegalese</option>
                        <option>Serbian</option>
                        <option>Seychellois</option>
                        <option>Sierra Leonean</option>
                        <option>Singapore</option>
                        <option>Slovak</option>
                        <option>Slovenian</option>
                        <option>Somali</option>
                        <option>South African</option>
                        <option>Spanish</option>
                        <option>Sri Lankan</option>
                        <option>Sudanese</option>
                        <option>Surinamese</option>
                        <option>Swazi</option>
                        <option>Swedish</option>
                        <option>Swiss</option>
                        <option>Syrian</option>
                        <option>Taiwanese</option>
                        <option>Tajikistani</option>
                        <option>Tanzanian</option>
                        <option>Thai</option>
                        <option>Timorese</option>
                        <option>Togolese</option>
                        <option>Tongan</option>
                        <option>Trinidadian</option>
                        <option>Tunisian</option>
                        <option>Turkish</option>
                        <option>Turkmen</option>
                        <option>Ugandan</option>
                        <option>Ukrainian</option>
                        <option>Uruguayan</option>
                        <option>Uzbekistani</option>
                        <option>Venezuelan</option>
                        <option>Vietnamese</option>
                        <option>Yemeni</option>
                        <option>Zambian</option>
                        <option>Zimbabwean</option>
                        <option>Other</option>
                    </select>
                </div>
            </div>

            <div class='col-sm-6'>    
                <div class='form-group'>
                    <label for="hotelname">Place for Pick up  <span class="red">*</span> </label>
                    <input class="form-control" type="text" required name="hotelname" id="hotelname">
                </div>
            </div>

            <div class='col-sm-6'>    
                <div class='form-group'>
                    <label for="destinations">Place for Destinations  </label>
                    <input class="form-control" type="text" required name="destinations" id="destinations">
                </div>
            </div>

            	


            <div class='col-sm-6'>    
                <div class='form-group'>
                    <label for="pickuptime">Pick-Up Date & Time <span class="red">*</span>  </label>
                    <input class="form-control datepicker" autocomplete="off" required type="text" id="pickuptime" name="pickuptime" data-role="datetime">
                </div>
            </div>

             <div class='col-sm-6'>    
                <div class='form-group'>
                    <label for="flightnumber">Flight Number  </label>
                    <input class="form-control"    type="text" id="flightnumber" name="flightnumber">
                </div>
            </div>
<div class='col-sm-6'>    
                <div class='form-group'>
                    <label for="roomnumber">Room Number  </label>
                    <input class="form-control" type="text" required name="roomnumber" id="roomnumber">
                </div>
            </div>

            <div class='col-sm-3'>    
                <div class='form-group'>
                    <label for="adult">Adult <span class="red">*</span>  </label>
                    <input class="form-control" required value="1" max="<?= $maxpeople ?>" type="number" name="adult" id="adult">
                </div>
            </div>

            <div class='col-sm-3'>    
                <div class='form-group'>
                    <label for="child">Child <span class="red">*</span>  </label>
                    <input class="form-control" required value="0" max="<?= $maxpeople ?>" type="number" name="child" id="child">
                </div>
            </div>

            <div class='col-sm-12'>    
                <div class='form-group'>
                    <label for="remark">Remark</label>
                    <textarea class="form-control" rows="5" name="remark" id="remark"></textarea>
                </div>
            </div>

            <div class='col-sm-12'>    
                <div class='form-group'>
                    <label for="remark">Captcha</label>
                    <img src="captcha.php" id="captcha" />
                    <div>
                        <a href="#" onclick="
                                document.getElementById('captcha').src = 'captcha.php?' + Math.random();
                                document.getElementById('captcha-form').focus();"
                           id="change-image">Not readable? Change text.</a>
                    </div>
                    <input required="" type="text" class="form-control" name="captcha" id="captcha-form" autocomplete="off" placeholder="Input Capcha" />
                </div>
            </div>

            <div class='col-sm-12 text-center'>    
                <div class='form-group'>
                    <button type="submit" class="btn btn-primary btn">Book Now</button>
                </div>
            </div>
        </div>
    </form>
</div>