<head>
    <?php
    $urlname = "http://$_SERVER[HTTP_HOST]";
    $actual_link = "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
    if (!isset($_tour_img) || $_tour_img == "") {
        $_tour_img = "images/tours/phuket-beaches-3.jpg";
    } else {
        $_tour_img = "images/tours/" . $_tour_img;
    }
    ?>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="<?= ($_title2 == '' ? '' : $_title2 . ' - ') . $_webname ?> - Phuket Transfers Service ,Phuket Tours, Airport Transfer. Best Price Guarantee." />
    <meta name="keywords" content="phuket airport transfer, phuket transfer service,family van,taxi-minibus, phuket ground transport, airport transfer phuket, phuket tours, phuket transfer, phuket taxi, phuket airport transportation, phuket private mini-bus, see you travel, good driver in phuket" />
    <meta name="news_keywords" content="<?= $_webname ?>" />
    <meta name="author" content="<?= $_webname ?>">
    <meta name="distribution" CONTENT="Global">
    <meta name="rating" CONTENT="General">
    <meta name="language" CONTENT="en-US">
    <meta name="COPYRIGHT" CONTENT="<?= $_webname ?>">
    <meta name="creator" content="phuketwebmodern.com" />
    <meta name="publisher" content="<?= $urlname ?>" />	
    <meta name="robots" content="index, follow">    
    <meta name="google-site-verification" content="2IUm5-ZZYkBsjXNcHRJSjVyAr_ouDP3KurwYfDY9WZ8" />
    <!-- <meta name="google-site-verification" content="8AgYAubkPUUUbtQU4yoCdUNwQ3fwVOnWUtyHLrsLJy0" /> -->
    <!-- for Facebook -->          

    <meta property="og:url" content="<?= $actual_link ?>" />
    <meta property="og:type" content="article" />
    <meta property="og:title" content="<?= ($_title2 == '' ? '' : $_title2 . ' - ') . $_webname ?>" />
    <meta property="og:description" content="<?= ($_title2 == '' ? '' : $_title2 . ' - ') . $_webname ?> - Phuket Transfers Service ,Phuket Tours, Airport Transfer." />
    <meta property="og:image" content="<?= $_domain .''. $_tour_img ?>" />
    <meta property="og:image:width" content="450"/>
    <meta property="og:image:height" content="298"/>
    <!-- meta facebook -->
    <!--<meta property="og:image" content="images/page-1_slide1.jpg" />-->
    <!--<meta property="og:description" content="<?= ($_title2 == '' ? '' : $_title2 . ' - ') . $_webname ?> - Phuket Transfers Service, Phuket Tours. Best Price Guarantee." />-->
    <!--<meta property="fb:admins" content="100003864744431" />-->

    <link rel="icon" href="images/logo.png" type="image/x-icon">

    <title><?= ($_title2 == '' ? '' : $_title2 . ' - ') . $_webname ?></title>

    <!-- <link rel="stylesheet" type="text/css" href="assets/css/bootstrap.min.css"> -->
    <link rel="stylesheet" type="text/css" href="assets/css/font-awesome.min.css">
    <link rel="stylesheet" type="text/css" href="assets/css/li-scroller.css">
    <link rel="stylesheet" type="text/css" href="assets/css/slick.css">
    <link rel="stylesheet" type="text/css" href="assets/css/weather.css">
    <link rel="stylesheet" type="text/css" href="assets/css/jquery.datetimepicker.css">
    <!-- 
    <link rel="stylesheet" type="text/css" href="assets/css/animate.css">
    <link rel="stylesheet" type="text/css" href="assets/css/theme.css">
    <link rel="stylesheet" type="text/css" href="assets/css/style.css">
    <link rel="stylesheet" type="text/css" href="assets/css/magnific-popup.css">
    <link rel="stylesheet" type="text/css" href="assets/css/jquery.datetimepicker.css">
    <link href="https://fonts.googleapis.com/css?family=Work+Sans:300,400,500,700,800" rel="stylesheet">
	 -->
	<!-- Animate.css -->
	<link rel="stylesheet" href="css/animate.css">
	<!-- Icomoon Icon Fonts-->
	<link rel="stylesheet" href="css/icomoon.css">
	<!-- Bootstrap  -->
	<link rel="stylesheet" href="css/bootstrap.css">
	<!-- mystyle  -->
	<link rel="stylesheet" href="assets/css/mystyle.css">

	<!-- Bootstrap  -->
	<link rel="stylesheet" href="css/bootstrap.css">
	<!-- Magnific Popup -->
	<link rel="stylesheet" href="css/magnific-popup.css">

	<!-- Owl Carousel  -->
	<link rel="stylesheet" href="css/owl.carousel.min.css">
	<link rel="stylesheet" href="css/owl.theme.default.min.css">
	<!-- Flexslider  -->
	<link rel="stylesheet" href="css/flexslider.css">

	<!-- Theme style  -->
	<link rel="stylesheet" href="css/style.css">

    <!--[if lt IE 9]>
    <script src="assets/js/html5shiv.min.js"></script>
    <script src="assets/js/respond.min.js"></script>
    <![endif]-->
    <script>
        (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
        (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
        m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
        })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

        ga('create', 'UA-81925247-1', 'auto');
        ga('send', 'pageview');

    </script>
</head>
