
<!-- Modernizr JS -->
<script src="js/modernizr-2.6.2.min.js"></script>
<!-- jQuery -->
<script src="js/jquery.min.js"></script>
<!-- jQuery Easing -->
<script src="js/jquery.easing.1.3.js"></script>
<!-- Bootstrap -->
<script src="js/bootstrap.min.js"></script>
<!-- Waypoints -->
<script src="js/jquery.waypoints.min.js"></script>
<!-- Stellar Parallax -->
<script src="js/jquery.stellar.min.js"></script>
<!-- Carousel -->
<script src="js/owl.carousel.min.js"></script>
<!-- Flexslider -->
<script src="js/jquery.flexslider-min.js"></script>
<!-- countTo -->
<script src="js/jquery.countTo.js"></script>
<!-- Magnific Popup -->
<script src="js/jquery.magnific-popup.min.js"></script>
<script src="js/magnific-popup-options.js"></script>
<!-- Main -->
<script src="js/main.js"></script>
<script src="assets/js/jquery.datetimepicker.full.min.js"></script>
<script src="assets/js/wow.min.js"></script>
<script src="assets/js/slick.min.js"></script>
<script src="assets/js/jquery.li-scroller.1.0.js"></script>
<!-- <script src="assets/js/custom.js"></script> -->
<script src="assets/js/jquery.simpleWeather.js" type="text/javascript"></script>
<script src="assets/js/jquery.isloading.min.js" type="text/javascript"></script>
<!-- <script src="assets/js/custom/app.js" type="text/javascript"></script> -->

<script type="text/javascript">
	$(function () { 
	 $( "#pickuptime" ).datetimepicker();
    console.log($('.my-div-off'));
     setInterval(function() {
            var isWhite = $('.my-div-off').css('border-color') == 'rgb(255, 255, 255)';
            $('.my-div-off').css({ 'border-color' : isWhite ? 'red' : '#fff' });
    }, 1000);
});

</script>