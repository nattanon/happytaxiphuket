<!DOCTYPE HTML>
<?php session_start() ?>
<!--DB-->
<?php
include 'conf/getGeneral.php';
$_title2 = "";
?>
<html>
	<?php include 'inc/head.inc.php'; ?>
	<body>
	<?php include 'pages/header.php' ?> 

	<aside id="fh5co-hero" class="js-fullheight">
		<div class="flexslider js-fullheight">
			<ul class="slides">
				<?php
					$tour_r1 = selects('tour', "WHERE slider = 1 ORDER BY RAND() LIMIT 5", '');
					$ti = 0;
					foreach ($tour_r1 as $tr) {
						$url = "tour-" . $tr['id'] . "-" . replace_blank($tr['name']) . "";
						$ti++;
				?>
					<li style="background-image: url(images/tours/<?= $tr['img_home'] ?>);">
						<div class="overlay-gradient"></div>
						<div class="container">
							<div class="row">
								<div class="col-md-8 col-md-offset-2 text-center js-fullheight slider-text">
									<div class="slider-text-inner">
										<h2><?= $tr['name'] ?></h2>
										<p><a class="btn btn-primary btn-lg" href="<?= $url ?>">View</a></p>
									</div>
								</div>
							</div>
						</div>
					</li>
				<?php } ?>
			</ul>
		</div>
	</aside>
	

	<div id="fh5co-content">
		<div class="video fh5co-video" style="background-image: url(images/index_profile.jpg);">
		</div>
		<div class="choose animate-box">
			<div class="fh5co-heading">
				<h2>Why Choose Us?</h2>
				<p>support fast development of tourism industry in Phuket. Since we have found lack of high quality transportation facility, we decided to launch new company which concentrates on providing quality and professional services. With our experiences and customer service knowledge, we could meet client’s expectation and be a part of impression for traveler, family, group tour and many organizations till present. </p>
			</div>
		</div>
	</div>

	<div id="fh5co-project" style="background-image:url(images/img_bg_1.jpg);
    background-position: center;
    padding-top:50px;
    padding-bottom:20px;
    background-repeat: no-repeat;
    background-size: cover;">
		<div class="container fix">
			<div class="row animate-box fix">
				<div class="col-md-8 col-md-offset-2 text-center fh5co-heading">
					<h2 style="color:White;">Best of Island Tours</h2>
					<!-- <p>Dignissimos asperiores vitae velit veniam totam fuga molestias accusamus alias autem provident. Odit ab aliquam dolor eius.</p> -->
				</div>
			</div>
		</div>
		<div class="container">
			<div class="row">
			<?php
					$tour_r2 = selects('tour', "WHERE tour_type = 1 ORDER BY RAND() LIMIT 6", '');
					$ti = 0;
					foreach ($tour_r2 as $tr) {
							$url = "tour-" . $tr['id'] . "-" . replace_blank($tr['name']) . "";
							$ti++;
							?>
								<div class="col-lg-4 col-md-4">
									<div class="fh5co-blog animate-box">
										<a href="<?= $url ?>"><img class="img-responsive ismax" src="images/tours/<?= $tr['img_home'] ?>" alt=""></a>
										<div class="blog-text">
											<h3 class="on-title"><a href="<?= $url ?>"><?= $tr['name'] ?></a></h3>
											<p class="on-content"><?= $tr['homedec'] ?></p>
											<a href="<?= $url ?>" class="btn btn-primary">Read More</a>
										</div> 
									</div>
								</div>
					<?php	} ?> 

			</div>
			
			<div class="col-md-12 text-center animate-box">
				<p><a class="btn btn-primary btn-lg btn-learn" href="phuket-1-islands-tours">View More</a></p>
			</div>
		</div>
	</div>

	<div id="fh5co-testimonial" class="fh5co-bg-section">
		<div class="container">
			<div class="row">
				<div class="col-md-10 col-md-offset-1">
					<div class="row animate-box">
						<div class="owl-carousel owl-carousel-fullwidth">
							<div class="item">
								<div class="testimony-slide active text-center">
									<figure>
										<img src="images/logo.png" alt="user">
									</figure>
									<h1>HAPPY TAXI PHUKET</h1>
									<blockquote>
										<p>&ldquo;If you are in the Patong, Kata beach, Karon Beach, Chalong areas you may have realized that hiring a reliable, honest, and good communicating driver poses several challenges. A generous number of destinations are worth your visit and you can feel that every possible convenience has been introduced&rdquo;</p>
									</blockquote>
								</div>
							</div>
							<div class="item">
								<div class="testimony-slide active text-center">
									<figure>
										<img src="images/logo.png" alt="user">
									</figure>
									<h1>HAPPY TAXI PHUKET</h1>
									<blockquote>
										<p>&ldquo;Phuket Tourism Happy Taxi Phuket&rdquo;</p>
									</blockquote>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div id="fh5co-project" style="background-image:url(images/pg-3.jpg);
    background-position: center;
    padding-top:50px;
    padding-bottom:20px;
    background-repeat: no-repeat;
    background-size: cover;">
		<div class="container fix">
			<div class="row animate-box fix">
				<div class="col-md-8 col-md-offset-2 text-center fh5co-heading">
					<h2 style="color:White;">Popular Advanture</h2>
					<!-- <p>Dignissimos asperiores vitae velit veniam totam fuga molestias accusamus alias autem provident. Odit ab aliquam dolor eius.</p> -->
				</div>
			</div>
		</div>
		<div class="container">
			<div class="row">
				<?php
					$tour_r3 = selects('tour', "WHERE tour_type = 2 ORDER BY RAND() LIMIT 6", '');
					$ti = 0;
					foreach ($tour_r3 as $tr) {
							$url = "tour-" . $tr['id'] . "-" . replace_blank($tr['name']) . "";
							$ti++;
							?>
							<div class="col-lg-4 col-md-4">
								<div class="fh5co-blog animate-box">
									<a href="<?= $url ?>"><img class="img-responsive ismax" src="images/tours/<?= $tr['img_home'] ?>" alt=""></a>
									<div class="blog-text">
										<h3 class="on-title"><a href="<?= $url ?>"><?= $tr['name'] ?></a></h3>
										<p class="on-content"><?= $tr['homedec'] ?></p>
										<a href="<?= $url ?>" class="btn btn-primary">Read More</a>
									</div> 
								</div>
							</div>
				<?php	} ?> 

			</div>
			<div class="col-md-12 text-center animate-box">
					<p><a class="btn btn-primary btn-lg btn-learn" href="phuket-2-islands-tours">View More</a></p>
			</div>
		</div>
	</div>
	
	<div id="fh5co-project">
		<div class="container fix">
			<div class="row animate-box">
				<div class="col-md-8 col-md-offset-2 text-center fh5co-heading">
					<h2>Favorite Photo</h2>
					<p>"Phuket, 'Pearl of Andaman' is Thailand's largest island. With its magnificent white sandy beaches and bay, its crystal blue water, its hospitable and friendly people has never failed to impress visitors, Phuket truly offers a memorable holiday.</p>
				</div>
			</div>
		</div>
		<div class="container">
			<div class="row">
				<div class="col-md-4 col-sm-6 text-center fh5co-project animate-box" data-animate-effect="fadeIn">
					<a ><img src="images/customer/รูปลูกค้า_181007_0009.jpg" alt="Can not find image Happy Taxi Phuket" class="img-responsive ismax">
					</a>
				</div>
				
				<div class="col-md-4 col-sm-6 text-center fh5co-project animate-box" data-animate-effect="fadeIn">
					<a ><img src="images/customer/รูปลูกค้า_181007_0092.jpg" alt="Can not find image Happy Taxi Phuket" class="img-responsive ismax">
					</a>
				</div>

				
				<div class="col-md-4 col-sm-6 text-center fh5co-project animate-box" data-animate-effect="fadeIn">
					<a ><img src="images/customer/รูปลูกค้า_181007_0104.jpg" alt="Can not find image Happy Taxi Phuket" class="img-responsive ismax">
					</a>
				</div>
				
				<div class="col-md-4 col-sm-6 text-center fh5co-project animate-box" data-animate-effect="fadeIn">
					<a ><img src="images/customer/รูปลูกค้า_181007_0107.jpg" alt="Can not find image Happy Taxi Phuket" class="img-responsive ismax">
					</a>
				</div>
				
				<div class="col-md-4 col-sm-6 text-center fh5co-project animate-box" data-animate-effect="fadeIn">
					<a ><img src="images/customer/55.jpg" alt="Can not find image Happy Taxi Phuket" class="img-responsive ismax">
					</a>
				</div>
				
				<div class="col-md-4 col-sm-6 text-center fh5co-project animate-box" data-animate-effect="fadeIn">
					<a ><img src="images/customer/รูปลูกค้า_181007_0056.jpg" alt="Can not find image Happy Taxi Phuket" class="img-responsive ismax">
					</a>
				</div>

			</div>
		</div>
	</div>

	<div id="fh5co-practice" style="background-image:url(images/img_bg_2.jpg);
    background-position: center;
    padding-top:50px;
    padding-bottom:20px;
    background-repeat: no-repeat;
    background-size: cover;">
		<div class="container fix">
			<div class="row animate-box fix">
				<div class="col-md-8 col-md-offset-2 text-center fh5co-heading">
					<h2 style="color:White;">Tour Type</h2>
				</div>
			</div>
			<div class="row">
				<?php
					$cat = selects("tour_type", "where status = 1 order by sort desc", "id,name");
					foreach ($cat as $val) {
						?>
						<div class="col-md-4 text-center animate-box">
							<a <?= "href='phuket-" . $val['id'] . "-" . replace_blank($val['name']) . "'"?> >
								<div class="services fix-size">
									<span class="icon">
										<i class="icon-heart"></i>
									</span>
									<div class="desc">
										<h3><?= $val['name'] ?></h3>
									</div>
								</div>
							</a>
						</div>
				<?php } ?>
			</div>
		</div>
	</div>

	<!-- <div id="fh5co-consult">
		<div class="video fh5co-video" style="background-image: url(images/video.jpg);">
		</div>
		<div class="choose animate-box">
			<div class="fh5co-heading">
				<h2>Free Legal Consultation</h2>
			</div>
			<form action="#">
				<div class="row form-group">
					<div class="col-md-6"> 
						<input type="text" id="fname" class="form-control" placeholder="Your firstname">
					</div>
					<div class="col-md-6"> 
						<input type="text" id="lname" class="form-control" placeholder="Your lastname">
					</div>
				</div>

				<div class="row form-group">
					<div class="col-md-12"> 
						<input type="text" id="email" class="form-control" placeholder="Your email address">
					</div>
				</div>

				<div class="row form-group">
					<div class="col-md-12"> 
						<input type="text" id="subject" class="form-control" placeholder="Your subject of this message">
					</div>
				</div>

				<div class="row form-group">
					<div class="col-md-12"> 
						<textarea name="message" id="message" cols="30" rows="10" class="form-control" placeholder="Say something about us"></textarea>
					</div>
				</div>
				<div class="form-group">
					<input type="submit" value="Send Message" class="btn btn-primary">
				</div>

			</form>	
		</div>
	</div> -->

	<!-- <div id="fh5co-blog" class="fh5co-bg-section">
		<div class="container">
			<div class="row animate-box">
				<div class="col-md-8 col-md-offset-2 text-center fh5co-heading">
					<h2>Recent Post</h2>
					<p>Dignissimos asperiores vitae velit veniam totam fuga molestias accusamus alias autem provident. Odit ab aliquam dolor eius.</p>
				</div>
			</div>
			<div class="row">
				<div class="col-lg-4 col-md-4">
					<div class="fh5co-blog animate-box">
						<a href="#"><img class="img-responsive" src="images/project-4.jpg" alt=""></a>
						<div class="blog-text">
							<span class="posted_on">Nov. 15th</span>
							<span class="comment"><a href="">21<i class="icon-speech-bubble"></i></a></span>
							<h3><a href="#">Legal Consultation</a></h3>
							<p>Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts.</p>
							<a href="#" class="btn btn-primary">Read More</a>
						</div> 
					</div>
				</div>
				<div class="col-lg-4 col-md-4">
					<div class="fh5co-blog animate-box">
						<a href="#"><img class="img-responsive" src="images/project-2.jpg" alt=""></a>
						<div class="blog-text">
							<span class="posted_on">Nov. 15th</span>
							<span class="comment"><a href="">21<i class="icon-speech-bubble"></i></a></span>
							<h3><a href="#">Criminal Case</a></h3>
							<p>Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts.</p>
							<a href="#" class="btn btn-primary">Read More</a>
						</div> 
					</div>
				</div>
				<div class="col-lg-4 col-md-4">
					<div class="fh5co-blog animate-box">
						<a href="#"><img class="img-responsive" src="images/project-3.jpg" alt=""></a>
						<div class="blog-text">
							<span class="posted_on">Nov. 15th</span>
							<span class="comment"><a href="">21<i class="icon-speech-bubble"></i></a></span>
							<h3><a href="#">Business Law</a></h3>
							<p>Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts.</p>
							<a href="#" class="btn btn-primary">Read More</a>
						</div> 
					</div>
				</div>
			</div>
		</div>
	</div> -->

	<!-- <div id="fh5co-about">
		<div class="container">
			<div class="row animate-box">
				<div class="col-md-8 col-md-offset-2 text-center fh5co-heading">
					<h2>Our Attorneys</h2>
					<p>Dignissimos asperiores vitae velit veniam totam fuga molestias accusamus alias autem provident. Odit ab aliquam dolor eius.</p>
				</div>
			</div>
			<div class="row">
				<div class="col-md-4 col-sm-4 text-center animate-box" data-animate-effect="fadeIn">
					<div class="fh5co-staff">
						<img src="images/user-2.jpg" alt="Free HTML5 Templates by gettemplates.co">
						<h3>Jean Smith</h3>
						<strong class="role">Counsel</strong>
						<p>Quos quia provident consequuntur culpa facere ratione maxime commodi voluptates id repellat velit eaque aspernatur expedita. Possimus itaque adipisci.</p>
						<ul class="fh5co-social-icons">
							<li><a href="#"><i class="icon-facebook"></i></a></li>
							<li><a href="#"><i class="icon-twitter"></i></a></li>
							<li><a href="#"><i class="icon-dribbble"></i></a></li>
							<li><a href="#"><i class="icon-github"></i></a></li>
						</ul>
					</div>
				</div>
			</div>
		</div>
	</div> -->

	<!-- <div id="fh5co-started" style="background-image:url(images/img_bg_2.jpg);">
		<div class="overlay"></div>
		<div class="container">
			<div class="row animate-box">
				<div class="col-md-8 col-md-offset-2 text-center fh5co-heading">
					<h2>Legal Advice</h2>
					<p>We help people effectively fight their offenders back and successfully defend their own stand!</p>
				</div>
			</div>
			<div class="row animate-box">
				<div class="col-md-8 col-md-offset-2 text-center">
					<p><a href="#" class="btn btn-default btn-lg">Consultation</a></p>
				</div>
			</div>
		</div>
	</div> -->

	
		<?php include 'pages/footer.php'; ?>
		<?php include 'inc/jsfoot.inc.php'; ?>
	</body>
</html>

