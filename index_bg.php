<?php session_start() ?>
<!--DB-->

FFFFFF
<?php
include 'conf/getGeneral.php';
$_title2 = "";
?>
<!DOCTYPE html>
<html>
    <?php include 'inc/head.inc.php'; ?>
    <body>
        <!--        <div id="preloader">
                    <div id="status">&nbsp;</div>
                </div>-->
        <a class="scrollToTop" href="#"><i class="fa fa-angle-up"></i></a>
        <div class="container">
            <div class="box_wrapper">
                <?php include 'pages/header.php' ?>
                <div class="latest_newsarea"></div>
                <section id="contentbody">
                    <div class="row">
                        <div class="col-lg-3 col-md-3 col-sm-4 col-xs-12">
                            <?php include 'pages/home_left.php'; ?>
                        </div>
                        <div class="col-lg-7 col-md-7 col-sm-8 col-xs-12">
                            <h3>Welcome to <?=$_webname?> </h3>
                            <?php include 'pages/home.php'; ?>
                        </div>
                        <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12">
                            <?php include 'pages/home_right.php'; ?>
                        </div>
                    </div>
                </section>
                <footer id="footer">
                    <?php include 'pages/footer.php'; ?>
                    <?php include 'pages/footer_copyright.php'; ?>
                </footer>
            </div>
        </div>
        <?php include 'inc/jsfoot.inc.php'; ?>
    </body>
</html>
