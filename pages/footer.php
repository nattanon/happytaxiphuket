<footer id="fh5co-footer" role="contentinfo" style="padding-bottom: 0;">
		<div class="container">
			<div class="row row-pb-md">
				<div class="col-md-3 fh5co-widget">
					<h4>Follow By Fanpage</h4>
					<script>
						(function(d, s, id) {
							var js, fjs = d.getElementsByTagName(s)[0];
							if (d.getElementById(id)) return;
							js = d.createElement(s); js.id = id;
							https://www.facebook.com/Happy-Taxi-Phuket-263685027624328/
							js.src = "//connect.facebook.net/th_TH/sdk.js#xfbml=1&version=v2.9&appId=1641602046084647";
							fjs.parentNode.insertBefore(js, fjs);
						}(document, 'script', 'facebook-jssdk'));
					</script>
					<iframe src="https://www.facebook.com/plugins/page.php?href=https%3A%2F%2Fwww.facebook.com%2FHappy-Taxi-Phuket-263685027624328%2F&tabs=timeline&width=340&height=500&small_header=false&adapt_container_width=true&hide_cover=false&show_facepile=true&appId=830795167131739" width="250" height="240" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowTransparency="true" allow="encrypted-media"></iframe>
				</div>
				
				<div class="col-md-3 col-md-push-1">
					<h4>Tripadvisor</h4>
            <div id="TA_cdswritereviewlg528" class="TA_cdswritereviewlg">
						<img style="" src="images/trip.jpg" alt="Can not find image Happy Taxi Phuket" class="img-responsive ismax">

            </div>
          				</div>

				<div class="col-md-3 col-md-push-1">
					<h4>Contact Information</h4>
					
					<ul class="fh5co-footer-links">
						<li>35/9 Baan Chao Fah nougat Garden Home 5 (beside Thalang) Moo 3 Tambol vichit, Amphur muang, Phuket 83000, Thailand</li>
						<li><a href="mailto:info@happytaxiphuket.com">info@happytaxiphuket.com</a></li>
						<li><a href="tel://0950197638">Tel. 0950197638</a></li>
						<li><a href="tel://0896529296">Tel. 0896529296</a></li>
						<li><a href="Wechat://dl/chat?app0950197638">Wechat ID: app0950197638</a></li>
						<li><a href="https://api.whatsapp.com/send?phone=0950197638">Whatsapp ID: 0950197638</a></li>
					</ul> 
				</div>

				<div class="col-md-3 col-md-push-1">
					<h4>Map</h4>
					<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3952.26026067751!2d98.34658031477858!3d7.867810994331419!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x0!2zN8KwNTInMDQuMSJOIDk4wrAyMCc1NS42IkU!5e0!3m2!1sen!2sth!4v1540203047032" width="250" height="240" frameborder="0" style="border:0" allowfullscreen></iframe>
				</div>

			</div>
			<div class="row copyright">
				<div class="col-md-12 text-center">
					<p>
						<small class="block">&copy;Copyright &copy; 2018 <?= $_webname ?>.</small> 
						<small class="block"> Site by <a href="http://www.phuketwebmodern.com">Phuket Web Modern</a></small>
					</p>
				</div>
			</div>

		</div>
	</footer>
	</div>

	<div class="gototop js-top">
		<a href="#" class="js-gotop"><i class="icon-arrow-up"></i></a>
	</div>
