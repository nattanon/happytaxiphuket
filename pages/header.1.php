<header id="header">
    <div class="header_top">
        <nav class="navbar navbar-default" role="navigation">
            <div class="container-fluid">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar"> <span class="sr-only">Toggle navigation</span> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </button>
                </div>
                <div id="navbar" class="navbar-collapse collapse">
                    <ul class="nav navbar-nav custom_nav">

                        <li 
                        <?php
                        if ($_title2 == '') {
                            echo 'class="active"';
                        }
                        ?>><a href=".">Home</a></li>
                        <li <?php
                        if ($_title2 == 'Airport Transfers') {
                            echo 'class="active"';
                        }
                        ?>><a href="airport-transfers">Airport Transfers</a></li>
                        <li <?php
                        if ($_title2 == 'Tours') {
                            echo 'class="active"';
                        }
                        ?>><a href="tours">Tours</a></li>
                        <li <?php
                        if ($_title2 == 'Islands Tours') {
                            echo 'class="active"';
                        }
                        ?>><a href="phuket-1-islands-tours">Island Tours</a></li>
                        <li <?php
                        if ($_title2 == 'Adventure Tour') {
                            echo 'class="active"';
                        }
                        ?>><a href="phuket-2-adventure-tour">Advanture Tours</a></li>
                        <li <?php
                        if ($_title2 == 'About') {
                            echo 'class="active"';
                        }
                        ?>><a href="about">About</a></li>
                        <li <?php
                        if ($_title2 == 'Contact us') {
                            echo 'class="active"';
                        }
                        ?>><a href="contact">Contact</a></li>
                    </ul>
                </div>
            </div>
        </nav>
        <!--        <div class="header_search">
                    <button id="searchIcon"><i class="fa fa-search"></i></button>
                    <div id="shide">
                        <div id="search-hide">
                            <form action="#">
                                <input type="text" size="40" placeholder="Search here ...">
                            </form>
                            <button class="remove"><span><i class="fa fa-times"></i></span></button>
                        </div>
                    </div>
                </div>-->
    </div>
    <div class="header_bottom">
        <div class="logo_area"><a class="logo" href="."><img src="images/logo_head.png" class="img-responsive" title="<?= $_webname ?>"></a></div>
        <div class="head_image"><img src="images/head_img.png" class="img-responsive"></div>
        <div class="top_addarea"> 
            <ul class="contact_head">
                <li><img src="images/icon/phone.png"> : <a href="tel:<?= $_webtel ?>"><?= $_webtel ?></a></li>
                <li><img src="images/icon/gmail.png"> : <a href="mailto:<?= $_webmail ?>"><?= $_webmail ?></a></li>
                <li><img src="images/icon/fb.png"> : <a href="https://www.facebook.com/friendlytaxiphuket/">Friendly Taxi & Tours Phuket</a></li>

                <li class="linetop">
                    <span class="col-md-4">
                        <a href="http://line.me/ti/p/~friendlytaxiphuket"><img src="images/icon/line.png"> </a>
                    </span>
                    <span class="col-md-4">
                        <img src="images/icon/wc.png"><a></a>
                    </span>
                    <span class="col-md-4">
                        <img src="images/icon/wa.png"><a></a>
                    </span>
            </ul>
            <!--<img src="images/addbanner_728x90_V1.jpg" alt="">-->
        </div>
        <div class="social_area_top">
            <ul class="social_area_mobile">
                <li> <a  class="tootlip" data-toggle="tooltip" data-placement="top"  data-original-title="Call now" href="tel:<?= $_webtel ?>"><img src="images/icon/phone.png"></a></li>
                <li><a class="tootlip" data-toggle="tooltip" data-placement="top"  data-original-title="Line" href="http://line.me/ti/p/~friendlytaxiphuket"><img src="images/icon/line.png"></a></li>
                <li><a class="tootlip" data-toggle="tooltip" data-placement="top"  data-original-title="Facebook" href="https://www.facebook.com/friendlytaxiphuket/"><img src="images/icon/fb.png"></a></li>
                <li><a class="tootlip" data-toggle="tooltip" data-placement="top"  data-original-title="Wechat" href="#"><img src="images/icon/wc.png"></a></li>
                <li><a class="tootlip" data-toggle="tooltip" data-placement="top"  data-original-title="What app" href="#"><img src="images/icon/wa.png"></a></li>
            </ul>
        </div>
    </div>
    <div class="ipadsocial">
        <div class="social_area_top">
            <ul class="social_area_mobile">
                <li> <a  class="tootlip" data-toggle="tooltip" data-placement="top"  data-original-title="Call now" href="tel:<?= $_webtel ?>"><img src="images/icon/phone.png"></a></li>
                <li><a class="tootlip" data-toggle="tooltip" data-placement="top"  data-original-title="Line" href="http://line.me/ti/p/~friendlytaxiphuket"><img src="images/icon/line.png"></a></li>
                <li><a class="tootlip" data-toggle="tooltip" data-placement="top"  data-original-title="Facebook" href="https://www.facebook.com/friendlytaxiphuket/"><img src="images/icon/fb.png"></a></li>
                <li><a class="tootlip" data-toggle="tooltip" data-placement="top"  data-original-title="Wechat" href="#"><img src="images/icon/wc.png"></a></li>
                <li><a class="tootlip" data-toggle="tooltip" data-placement="top"  data-original-title="What app" href="#"><img src="images/icon/wa.png"></a></li>
            </ul>
        </div>
    </div>
</header>