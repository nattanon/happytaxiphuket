


	<div class="fh5co-loader"></div>
	
	<div id="page">
	<nav class="fh5co-nav" role="navigation">
		<div class="top-menu">
			<div class="container">
				<div class="row">
					<div class="col-xs-2">
						<div id="fh5co-logo"><a href="/"><img src="images/logo_head.png" width="200"></a></div>
						<!-- <h1>HAPPY TAXI</h1> -->
					</div>
					<div class="col-xs-10 text-right menu-1">
						<ul>
							<li  <?php
                            if ($_title2 == '') {
                                echo 'class="active"';
                            }
                            ?>>
                                <a href="/">Home</a>
							</li>
							<li <?php
							if ($_title2 == 'Airport Transfers') {
								echo 'class="active"';
							}
							?>><a href="airport-transfers">Airport Transfers</a></li>
							<li class="has-dropdown <?php
							if ($_title2 == 'Tours') {
								echo 'active';
							}
							?>"><a href="tours">Tours</a>
							
								<ul class="dropdown">
									<?php
										$cat = selects("tour_type", "where status = 1 order by sort desc", "id,name");
										foreach ($cat as $val) {
											?>
											<li><a <?= "href='phuket-" . $val['id'] . "-" . replace_blank($val['name']) . "'"?>><?= $val['name'] ?></a></li>
									<?php } ?>
								</ul>
						
							</li>
							<li class="has-dropdown <?php
							if ($_title2 == 'Islands Tours') {
								echo 'active';
							}
							?>"><a href="phuket-1-islands-tours">Island Tours</a>
							
								<ul class="dropdown">
									<?php
										$cat2 = selects("tour", "where tour_type = 1", "id,name");
										foreach ($cat2 as $val) {
											?>
											<li><a <?= "href='tour-" . $val['id'] . "-" . replace_blank($val['name']) . "'"?>><?= $val['name'] ?></a></li>
									<?php } ?>
								</ul>
						
							</li>
							<li class="has-dropdown <?php
							if ($_title2 == 'Adventure Tours') {
								echo 'active';
							}
							?>"><a href="phuket-2-adventure-tour">Advanture Tours</a>
								<ul class="dropdown">
									<?php
										$cat2 = selects("tour", "where tour_type = 2", "id,name");
										foreach ($cat2 as $val) {
											?>
											<li><a <?= "href='tour-" . $val['id'] . "-" . replace_blank($val['name']) . "'"?>><?= $val['name'] ?></a></li>
									<?php } ?>
								</ul>
							</li>
							<li <?php
							if ($_title2 == 'About') {
								echo 'class="active"';
							}
							?>><a href="about">About</a></li>
							<li <?php
							if ($_title2 == 'Contact us') {
								echo 'class="active"';
							}
							?>><a href="contact">Contact</a></li>
							<!-- <li><a href="practice.html">Practice Areas</a></li>
							<li><a href="won.html">Won Cases</a></li>
							<li class="has-dropdown">
								<a href="blog.html">Blog</a>
								<ul class="dropdown">
									<li><a href="#">Web Design</a></li>
									<li><a href="#">eCommerce</a></li>
									<li><a href="#">Branding</a></li>
									<li><a href="#">API</a></li>
								</ul>
							</li>
							<li><a href="about.html">About</a></li>
							<li><a href="contact.html">Contact</a></li> -->
						</ul>
					</div>
				</div>
				
			</div>
		</div>
	</nav>