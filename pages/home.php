<div class="row">
    <div class="middle_bar">
        <div class="featured_sliderarea">
            <div id="myCarousel" class="carousel slide" data-ride="carousel">
                <!--                <ol class="carousel-indicators">
                <?php
                ?>
                                                        <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
                                                        <li data-target="#myCarousel" data-slide-to="1"></li>
                                                        <li data-target="#myCarousel" data-slide-to="2"></li>
                                                        <li data-target="#myCarousel" data-slide-to="3"></li> 
                                </ol>-->
                <div class="carousel-inner" role="listbox">
                    <?php
                    $tour_r1 = selects('tour', "WHERE slider = 1 ORDER BY RAND()", '');
                    $ti = 0;
                    foreach ($tour_r1 as $tr) {
                        $url = "tour-" . $tr['id'] . "-" . replace_blank($tr['name']) . "";
                        $ti++;
                        ?>
                        <div class="item <?= ($ti == 1 ? 'active' : '') ?>"> <img style="width: 668px; height: 420px" src="images/tours/<?= $tr['img_home'] ?>" alt="">
                            <div class="carousel-caption">
                                <h1><a href="<?= $url ?>"> <?= $tr['name'] ?></a></h1>
                            </div>
                        </div>
                        <?php
                    }
                    ?>
                </div>
                <a class="left left_slide" href="#myCarousel" role="button" data-slide="prev"> <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span> </a> <a class="right right_slide" href="#myCarousel" role="button" data-slide="next"> <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span> </a></div>
        </div>
        <div style="padding: 20px" class="single_category wow fadeInDown">
            <p><img src="images/logo_head.png" class="img-responsive"></p>
            <p>If you are in the Patong, Kata beach, Karon Beach, Chalong areas you may have realized that hiring a reliable, honest, and good communicating driver poses several challenges. A generous number of destinations are worth your visit and you can feel that every possible convenience has been introduced</p>
        </div>
        <div class="single_category wow fadeInDown">
            <div class="category_title"> <a href="phuket-1-islands-tours">Best of Island Tours</a></div>
            <div class="single_category_inner">
                <ul class="catg_nav">
                    <?php
                    $tour_r2 = selects('tour', "WHERE tour_type = 1 ORDER BY RAND() LIMIT 4", '');
                    $ti = 0;
                    foreach ($tour_r2 as $tr) {
                        $url = "tour-" . $tr['id'] . "-" . replace_blank($tr['name']) . "";
                        $ti++;
                        ?>
                        <li>
                            <div class="catgimg_container"> <a class="catg1_img" href="<?= $url ?>"> <img src="images/tours/<?= $tr['img_home'] ?>" alt=""> </a></div>
                            <a class="catg_title" href="<?= $url ?>"> <?= $tr['name'] ?></a>
                            <p class="post-summary"><?= $tr['homedec'] ?></p>
                        </li>
                        <?php
                    }
                    ?>

                </ul>
            </div>
        </div>
        <div class="single_category  wow fadeInDown">
            <div class="category_title"> <a href="phuket-2-adventure-tour">Popular Advanture</a></div>
            <div class="single_category_inner">
                <ul class="catg_nav catg_nav2">
                    <?php
                    $tour_r3 = selects('tour', "WHERE tour_type = 2 ORDER BY RAND() LIMIT 4", '');
                    $ti = 0;
                    foreach ($tour_r3 as $tr) {
                        $url = "tour-" . $tr['id'] . "-" . replace_blank($tr['name']) . "";
                        $ti++;
                        ?>
                        <li>
                            <div class="catgimg_container"> <a class="catg1_img" href="#"> <img src="images/tours/<?= $tr['img_home'] ?>" alt="<?= $tr['name'] ?>"> </a></div>
                            <a class="catg_title" href="<?= $url ?>"> <?= $tr['name'] ?></a>
                            <p class="post-summary"><?= $tr['homedec'] ?></p>
                        </li>
                        <?php
                    }
                    ?>
                </ul>
            </div>
        </div>
    </div>
</div>