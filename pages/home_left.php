<div class="row">
    <div class="left_bar">
      

        <div class="single_leftbar wow fadeInDown">
            <h2><span>Tour Type</span></h2>
            <div class="singleleft_inner">
                <ul class="label_nav">
                    <?php
                    $cat = selects("tour_type", "where status = 1 order by sort desc", "id,name");
                    foreach ($cat as $val) {
                        echo "<li> <a href='phuket-" . $val['id'] . "-" . replace_blank($val['name']) . "'> " . $val['name'] . " </a></li>";
                    }
                    ?>
                </ul>
            </div>
        </div>
        <div class="single_leftbar wow fadeInDown _weather">
            <h2><span>Phuket Weather</span></h2>
            <div id="weather"></div>
        </div>
        <div class="single_leftbar wow fadeInDown">
            <ul class="nav nav-tabs custom-tabs" role="tablist">
                <li role="presentation" class="active"><a href="#home" aria-controls="home" role="tab" data-toggle="tab">Popular</a></li>
                <!--<li role="presentation"><a href="#profile" aria-controls="profile" role="tab" data-toggle="tab">Most Reader</a></li>-->

            </ul>
            <div class="tab-content">
                <div role="tabpanel" class="tab-pane fade in active" id="home">
                    <ul class="catg3_snav ppost_nav wow fadeInDown">
                        <?php
                        $sql = "select t.id,t.name,t.img_home,p.view from tour as t, popular_tour as p  where p.tour_id = t.id limit 8";
                        $query = mysql_query($sql);
                        while ($row = mysql_fetch_array($query)) {
                            $url = "tour-" . $row['id'] . "-" . replace_blank($row['name']) . "";
                            ?>
                            <li>
                                <div class="media"> <a class="media-left" href="<?= $url ?>"> <img src="images/tours/<?= $row['img_home'] ?>" alt=""> </a>
                                    <div class="media-body"> <a class="catg_title" href="<?= $url ?>"> <?= $row['name'] ?></a></div>
                                </div>
                            </li>
                            <?php
                        }
                        ?>
                    </ul>
                </div>
                <!--                <div role="tabpanel" class="tab-pane fade" id="profile">
                                    <ul class="catg3_snav ppost_nav wow fadeInDown">
                                        <li>
                                            <div class="media"> <a class="media-left" href="#"> <img src="images/70x70.jpg" alt=""> </a>
                                                <div class="media-body"> <a class="catg_title" href="#"> Aliquam malesuada diam eget turpis varius</a></div>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="media"> <a class="media-left" href="#"> <img src="images/70x70.jpg" alt=""> </a>
                                                <div class="media-body"> <a class="catg_title" href="#"> Aliquam malesuada diam eget turpis varius</a></div>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="media"> <a class="media-left" href="#"> <img src="images/70x70.jpg" alt=""> </a>
                                                <div class="media-body"> <a class="catg_title" href="#"> Aliquam malesuada diam eget turpis varius</a></div>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="media"> <a class="media-left" href="#"> <img src="images/70x70.jpg" alt=""> </a>
                                                <div class="media-body"> <a class="catg_title" href="#"> Aliquam malesuada diam eget turpis varius</a></div>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="media"> <a class="media-left" href="#"> <img src="images/70x70.jpg" alt=""> </a>
                                                <div class="media-body"> <a class="catg_title" href="#"> Aliquam malesuada diam eget turpis varius</a></div>
                                            </div>
                                        </li>
                                    </ul>
                                </div>-->

            </div>
        </div>
        <!-- <div class="single_leftbar wow fadeInDown airport_transfers">
            <h2><span>Mr.Beer</span></h2>
            <div class="singleleft_inner"> <a href="/about"><img alt="" src="http://friendlytaxiphuket.com/images/web/web_7560.jpg"></a></div>
        </div> -->
    </div>
</div>