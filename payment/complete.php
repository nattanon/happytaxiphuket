<?php
include 'include/database.php';
include 'include/function.php';
include 'include/seting.php';
$token = $_REQUEST['token'];
$success = false;
$sql = "Select max(id) from receive where token = '$token' ";
$obj = mysql_query($sql);
$row = mysql_fetch_array($obj);
$lastID = $row[0];
$sql_update = "UPDATE receive set `status` = 1 where id = $lastID";
if (mysql_query($sql_update)) {
    $success = true;
    sentemail($lastID);
}
?>
<!doctype html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Checkout Complete</title>
        <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
        <style>
            .cliente {
                margin-top:10px;
                border: #cdcdcd medium solid;
                border-radius: 10px;
                -moz-border-radius: 10px;
                -webkit-border-radius: 10px;
            }
            .transfers_price{
                color: red;
            }
            .red{
                color: red;
                font-weight: bold;
            }
        </style>
    </head>
    <body>
        <div style="margin-top:120px">
            <div class="container" id="content">
                <div class="form-group">
                    <div class="row">
                        <div class="col-sm-12 text-center">
                            <?php
                            if ($success == true) {
                                echo "<script>setTimeout('Redirect()',5000);</script>";
                                ?>
                                <div class="col-sm-12 alert alert-info">
                                    <img src="source/images/logo.png">
                                    <h1>Your Payment was successful</h1> 
                                    <p>Thank you</p>
                                    <p style="color:seagreen;font-size: 12px">
                                        The system send Receipt to your Email.Please check your email inbox or junk box <br/>
                                        System will redirect to main page in 5 seconds.
                                    </p>
                                </div>
                                <?php
                            } else {
                                echo "<script>alert('Paypal no return value!')</script>";
                                echo "<script>setTimeout('Redirect()',5000);</script>";
                            }
                            ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--</div>-->
        <script src="js/jquery-1.12.0.min.js"></script>
        <script src="js/bootstrap.js"></script>
        <script src="js/jquery.datetimepicker.min.js"></script>
        <script src="js/jquery.datetimepicker.full.min.js"></script>
        <script src="js/jquery-ui-1.9.2.custom.min.js"></script>
        <script src="js/jquery.form-validator.min.js"></script>
        <script src="js/fn/zindex.js"></script>
        <script src="js/app.js"></script>
        <script>
            function Redirect()
            {
                window.location = '<?= $_url_company ?>';
            }
        </script>
    </body>
</html>
