<?php

function curl($url) {
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    $data = curl_exec($ch);
    curl_close($ch);

    return $data;
}

$G_feed = 'http://api.fixer.io/latest?base=USD';
$G_paypal_fee = 1.044; /* 4.4% */
$G_paypal_fee_display = ($G_paypal_fee - 1) * 100;
$G_jsonx = curl($G_feed);
$G_getUSD = json_decode($G_jsonx, true);
$G_paypal_currency = $G_getUSD['rates']['THB'];
$G_paypal_fee2 = ceil(0.30 * ceil($G_paypal_currency)); /* 0.30USD */
$G_paypal_currency_updated = $G_getUSD['date'];
?>
