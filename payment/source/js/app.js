$(function () {
    $('.ui.dropdown').dropdown();
    $('#view_tour').click(function () {
        var url = $(this).data('link');
        window.open(url, '_blank');
    });
    $('[data-role="date"]').datepicker().datepicker("option", "dateFormat", 'dd-mm-yy');
    $('[name="pre_amount"]').ForceNumericOnly();
    $('[name="pre_amount"]').keyup(function () {
        var fee144 = +($('#fee144').val());
        var fee030usd = +($('#fee030usd').val());
        var curInput = +($(this).val());
        var curTotal = curInput;
        // alert(curTotal);
        // var curTotal = (curInput * fee144) + fee030usd;
        $('#Amount').val(curTotal.toFixed(2));
        $('#subtotal').text(curTotal.toFixed(2));
    });
    $('[name="pre_amount"]').change(function () {
        $(this).trigger('keyup');
    });

    $('[data-content]').popup({
        inline: true,
        hoverable: true,
        position: 'bottom left',
        delay: {
            show: 300,
            hide: 800
        }
    });
    function openWindowWithPost() {
        var f = document.getElementById('payform');
        f.submit();
    }
    $('[name="payform"]').submit(function (e) {
        e.preventDefault();
        $.ajax({
            type: "POST",
            url: 'doSave.php',
            data: $(this).serialize(),
            success: function (data)
            {
                console.log(data);
                if (data === 'done') {
                    openWindowWithPost();
                }
            }
        });
    });

});
$(document).ready(function () {
    $('.ui.form')
            .form({
                fields: {
                    email: {
                        identifier: 'email',
                        rules: [
                            {
                                type: 'empty',
                                prompt: 'Please enter your e-mail'
                            },
                            {
                                type: 'email',
                                prompt: 'Please enter a valid e-mail'
                            }
                        ]
                    },
                    password: {
                        identifier: 'password',
                        rules: [
                            {
                                type: 'empty',
                                prompt: 'Please enter your password'
                            },
                            {
                                type: 'length[6]',
                                prompt: 'Your password must be at least 6 characters'
                            }
                        ]
                    }, last_name: {
                        identifier: 'last_name',
                        rules: [
                            {
                                type: 'empty',
                                prompt: 'Please enter your value'
                            }
                        ]
                    }, first_name: {
                        identifier: 'first_name',
                        rules: [
                            {
                                type: 'empty',
                                prompt: 'Please enter your value'
                            }
                        ]
                    }, country: {
                        rules: [
                            {
                                type: 'empty',
                                prompt: 'Please enter your value'
                            }
                        ]
                    }, packagetours: {
                        rules: [
                            {
                                type: 'empty',
                                prompt: 'Please enter your value'
                            }
                        ]
                    }, paidtype: {
                        rules: [
                            {
                                type: 'empty',
                                prompt: 'Please enter your value'
                            }
                        ]
                    }, pre_amount: {
                        identifier: 'pre_amount',
                        rules: [
                            {
                                type: 'number',
                                prompt: 'Please enter a valid number'
                            }, {
                                type: 'empty',
                                prompt: 'Please enter your value'
                            }
                        ]
                    }
                }
            })
            ;
})
        ;

//FN
jQuery.fn.ForceNumericOnly =
        function ()
        {
            return this.each(function ()
            {
                $(this).keydown(function (e)
                {
                    var key = e.charCode || e.keyCode || 0;
                    // allow backspace, tab, delete, enter, arrows, numbers and keypad numbers ONLY
                    // home, end, period, and numpad decimal
                    return (
                            key === 8 ||
                            key === 9 ||
                            key === 13 ||
                            key === 46 ||
                            key === 110 ||
                            key === 190 ||
                            (key >= 35 && key <= 40) ||
                            (key >= 48 && key <= 57) ||
                            (key >= 96 && key <= 105));
                });
            });
        };