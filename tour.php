<?php session_start() ?>
<!--DB-->
<?php
include 'conf/getGeneral.php';
error_reporting(E_ERROR | E_PARSE);

$id = isset($_GET['id']) ? (int) $_GET['id'] : 0;
$tour = select('tour', "where id = $id", '');
$type = $tour['tour_type'];
//$cat = select('tour_type', "where id = 1", 'id,name');
$sql = "SELECT id,name FROM tour_type where id = 1";
$dbquery = mysql_query($sql);
$cats = mysql_fetch_array($dbquery);
$catlink = "phuket-" . $cats['id'] . "-" . replace_blank($cats['name']) . "";

$_title2 = $tour['name'];
$_tour_img = $tour['img_home'];
$_tour_img_1 = "images/tours/".$tour['img_1'];
$_tour_img_2 = "images/tours/".$tour['img_2'];
$_tour_img_3 = "images/tours/".$tour['img_3'];
$_tour_img_4 = "images/tours/".$tour['img_4'];
//Check Value
// $sql = "SELECT COUNT(id) FROM popular_tour WHERE tour_id = $id ";
// $ob = mysql_query($sql);
// $row = mysql_fetch_array($ob);
// if ($row[0] > 0) {
//     $sql_popular = "update popular_tour set view = (view+1) where tour_id = $id";
// } else {
//     $sql_popular = "insert into popular_tour (view,tour_id) values (1,$id)";
// }

// mysql_query($sql_popular);
// //$base_url = "http://localhost/happytaxiphuket/";
// ?>

<!DOCTYPE html>
<html>
    <?php include 'inc/head.inc.php'; ?>
    <body>
        <?php include 'pages/header.php' ?>
		
        <aside id="fh5co-hero" class="js-fullheight">
		<div class="flexslider js-fullheight">
			<ul class="slides">
		   	<li style="background-image: url(<?=$_tour_img;?>);">
		   		<div class="overlay-gradient"></div>
		   		<div class="container">
		   			<div class="col-md-10 col-md-offset-1 text-center js-fullheight slider-text">
		   				<div class="slider-text-inner desc">
		   					<h2 class="heading-section"><?=$tour[name];?></h2>
		   					<p class="fh5co-lead">Enjoy and have fun with <span style="color:yellow;">Happy Taxi Phuket</span>.</p>
		   				</div>
		   			</div>
		   		</div>
		   	</li>
		  	</ul>
	  	</div>
	</aside>
	<div class="container" style="    margin-top: 40px;  margin-bottom: 20px;">
			<div class="row">
			
			<div class="col-md-4 col-sm-6 text-center fh5co-project animate-box fadeIn animated-fast" data-animate-effect="fadeIn">
					<a><img src="<?=$_tour_img_1;?>" alt="Can not find image Happy Taxi Phuket" class="img-responsive ismax">
					</a>
				</div>
				<div class="col-md-4 col-sm-6 text-center fh5co-project animate-box fadeIn animated-fast" data-animate-effect="fadeIn">
					<a><img src="<?=$_tour_img_2;?>" alt="Can not find image Happy Taxi Phuket" class="img-responsive ismax">
					</a>
				</div>
				
				<div class="col-md-4 col-sm-6 text-center fh5co-project animate-box fadeIn animated-fast" data-animate-effect="fadeIn">
					<a><img src="<?=$_tour_img_3;?>" alt="Can not find image Happy Taxi Phuket" class="img-responsive ismax">
					</a>
				</div>

				
				<!--<div class="col-md-4 col-sm-6 text-center fh5co-project animate-box fadeIn animated-fast" data-animate-effect="fadeIn">
					<a><img src="<?=$_tour_img_4;?>" alt="Can not find image Happy Taxi Phuket" class="img-responsive ismax">
					</a>
				</div>-->

			</div>
			<div class="row">
			
				<div class="single_post_content">
                                            <div class="tour_description">
                                                <blockquote>
                                                    <?= $tour['homedec'] ?>
                                                </blockquote>
                                                <!--<img class="img-center" src="../images/400x300.jpg" alt="">-->
                                                <div class="wow fadeInDown animated">
                                                    <ul class="nav nav-tabs  " role="tablist">
                                                        <li class="active"><a data-toggle="tab" href="#dec">Description</a></li>
                                                        <?php
                                                        if ($tour['itinerary'] != '') {
                                                            echo '<li><a data-toggle="tab" href="#itr">Itinerary</a></li>';
                                                        }
                                                        if ($tour['tourinclude'] != '') {
                                                            echo '<li><a data-toggle="tab" href="#tic">Tour include</a></li>';
                                                        }
                                                        if ($tour['whattobting'] != '') {
                                                            echo '<li><a data-toggle="tab" href="#wtb">What To Bring</a></li>';
                                                        }
                                                        if ($tour['transfersfee'] != '') {
                                                            echo '<li><a data-toggle="tab" href="#tff">Transfers Fee</a></li>';
                                                        }
                                                        ?>
                                                    </ul>
                                                    <div class="tab-content" style="     padding: 15px;   margin: 15px 0px;">
                                                        <div id="dec" class="tab-pane fade in active">
                                                            <h3>Description</h3>
                                                            <p> <?= $tour['fulldes'] ?></p>
                                                        </div>
                                                        <div id="itr" class="tab-pane fade">
                                                            <?php
                                                            if ($tour['itinerary'] != '') {
                                                                ?>
                                                                <h3>Itinerary</h3>
                                                                <p><?= $tour['itinerary'] ?> </p>
                                                                <?php
                                                            }
                                                            ?>
                                                        </div>
                                                        <div id="tic" class="tab-pane fade">
                                                            <?php
                                                            if ($tour['tourinclude'] != '') {
                                                                ?>
                                                                <h3>Tour include</h3>
                                                                <p><?= $tour['tourinclude'] ?> </p>
                                                                <?php
                                                            }
                                                            ?>
                                                        </div>
                                                        <div id="wtb" class="tab-pane fade">
                                                            <?php
                                                            if ($tour['whattobting'] != '') {
                                                                ?>
                                                                <h3>What To Bring</h3>
                                                                <p><?= $tour['whattobting'] ?> </p>
                                                                <?php
                                                            }
                                                            ?>
                                                        </div>
                                                        <div id="tff" class="tab-pane fade">
                                                            <?php
                                                            if ($tour['transfersfee'] != '') {
                                                                ?>
                                                                <h3>Transfers Fee</h3>
                                                                <p><?= $tour['transfersfee'] ?> </p>
                                                                <?php
                                                            }
                                                            ?>
                                                        </div>
                                                        <hr/>
                                                        <?php
                                                        if ($tour['adult_net_price'] != 0) {
                                                            ?>
                                                            <div class="text-right">
                                                                <p>
                                                                <h4>Adult : <?= $tour['adult_net_price'] ?> THB</h4>
                                                                </p>
                                                                <p>
                                                                <h4>Child : <?= $tour['child_net_price'] ?> THB</h4>
                                                                </p>
                                                            </div>
                                                            <?php
                                                        }
                                                        ?>
                                                    </div>
                                                
                                            </div>

                                           
                                        </div>			
			</div>
			
			</div>
					
			<div class="row">
				<?php include 'inc/booking_form.php'; ?>
			</div>

    </div>                                        
        
        
        
    	<?php include 'pages/footer.php'; ?>  
        <?php include 'inc/jsfoot.inc.php'; ?>
    </body>
    <script>
    	$( "#booking_form" ).submit(function( event ) {
    		var data = $( this ).serialize();
    		var url = "inc/mailer.php";
    		console.log(data);
		 	$.ajax({
		        type: "post",
		        url: url,
		        dataType:"json",
		        data: data,
		        success: function (response) {
		            console.log(response);
		            if(response.status==0){
						alert(response.msg);
					}else{
						if(response.mail==true){
							alert("Success");
							location.reload();
						}
					}
		        }
		    });
		   /* $.post( url,data, function( response ) {
				 console.log(response);
			});*/
		  event.preventDefault();
		});
    </script>
</html>