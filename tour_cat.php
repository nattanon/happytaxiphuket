<?php session_start() ?>
<!--DB-->
<?php
include 'conf/getGeneral.php';

$id = isset($_GET['id']) ? (int) $_GET['id'] : 0;
$cat1 = select('tour_type', "where id = $id", 'id,name');
$_title2 = $cat1['name'];
?>
<!DOCTYPE html>
<html>
    <?php include 'inc/head.inc.php'; ?>
    <body>
        <?php include 'pages/header.php' ?> 
        <aside id="fh5co-hero" class="js-fullheight">
            <div class="flexslider js-fullheight">
                <ul class="slides">
                <?php 
                    $image_t = selects('tour', "where tour_type = " . $cat1['id'] . " ORDER BY RAND() LIMIT 1", '');
                ?>
                <li style="background-image: url(images/tours/<?= $image_t[0]['img_home'] ?>);">
                    <div class="overlay-gradient"></div>
                    <div class="container">
                        <div class="col-md-10 col-md-offset-1 text-center js-fullheight slider-text">
                            <div class="slider-text-inner desc">
                                <h2 class="heading-section"><?= $cat1['name'] ?></h2>
                                <p class="fh5co-lead">Enjoy and have fun with <span style="color:yellow;">Happy Taxi Phuket</span> on <?= $cat1['name'] ?>.</p>
                            </div>
                        </div>
                    </div>
                </li>
                </ul>
            </div>
        </aside>
        <div id="fh5co-project" class="fh5co-bg-section"
        >
        <!-- style="background-image:url(images/img_bg_1.jpg);
        background-position: center;
        padding-top:50px;
        padding-bottom:20px;
        background-repeat: no-repeat;
        background-size: cover;"> -->
            <div class="container">
                <div class="row">
                <?php
                        $tour_r2 = selects('tour', "where tour_type = " . $cat1['id'] . "", '');
                        $ti = 0;
                        foreach ($tour_r2 as $tr) {
                                $url = "tour-" . $tr['id'] . "-" . replace_blank($tr['name']) . "";
                                $ti++;
                                ?>
                                    <div class="col-lg-4 col-md-4">
                                        <div class="fh5co-blog animate-box">
                                            <a href="<?= $url ?>"><img class="img-responsive ismax" src="images/tours/<?= $tr['img_home'] ?>" alt=""></a>
                                            <div class="blog-text">
                                                <h3 class="on-title"><a href="<?= $url ?>"><?= $tr['name'] ?></a></h3>
                                                <p class="on-content"><?= $tr['homedec'] ?></p>
                                                <a href="<?= $url ?>" class="btn btn-primary">Read More</a>
                                            </div> 
                                        </div>
                                    </div>
                        <?php	} ?> 

                </div>
                <!-- <div class="col-md-12 text-center animate-box">
                    <p><a class="btn btn-primary btn-lg btn-learn" href="phuket-1-islands-tours">View More</a></p>
                </div> -->
            </div>
        </div>
        


        <?php include 'pages/footer.php'; ?>
        <?php include 'inc/jsfoot.inc.php'; ?>
    </body>
</html>