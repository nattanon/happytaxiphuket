<?php session_start() ?>
<!--DB-->
<?php
$_title2 = 'Tours';
include 'conf/getGeneral.php';
?>
<!DOCTYPE html>
<html>
    <?php include 'inc/head.inc.php'; ?>
    <body>
    
	    <?php include 'pages/header.php' ?> 
 
        <aside id="fh5co-hero" class="js-fullheight">
            <div class="flexslider js-fullheight">
                <ul class="slides">
                <li style="background-image: url(images/img_bg_1.jpg);">
                    <div class="overlay-gradient"></div>
                    <div class="container">
                        <div class="col-md-10 col-md-offset-1 text-center js-fullheight slider-text">
                            <div class="slider-text-inner desc">
                                <h2 class="heading-section">Tours</h2>
                                <p class="fh5co-lead">Enjoy and have fun with <span style="color:yellow;">Happy Taxi Phuket</span>. We have many tours to choose from.</p>
                            </div>
                        </div>
                    </div>
                </li>
                </ul>
            </div>
        </aside>
	
        <div id="fh5co-testimonial" class="fh5co-bg-section">
            <div class="container fix">
                <div class="row">
                    <?php
                        $cat = selects("tour_type", "where status = 1 order by sort desc", "id,name");
                        foreach ($cat as $key=>$val) {
                            ?>
                            <div class="col-md-4 text-center animate-box">
                                <a <?= "href='phuket-" . $val['id'] . "-" . replace_blank($val['name']) . "'"?> >
                                    <div class="services fix-size"> 
                                            <!-- <i class="icon-heart"></i>  -->
                                        <div class="desc">
                                            <h3><?=  $key + 1 ?>. <?= $val['name'] ?></h3>
                                        </div>
                                    </div>
                                </a>
                            </div>
                    <?php } ?>
                </div>
            </div>
        </div>
        
	
        <div id="fh5co-project">
            <div class="container fix">
                <div class="row animate-box">
                    <div class="col-md-8 col-md-offset-2 text-center fh5co-heading">
					    <h2>Popular Tours</h2>
                        <p>"Tours are popular and very impressive. We have a choice.</p>
                    </div>
                </div>
            </div>
            <div class="container">
                <div class="row">
                    <?php
                        $tour_r1 = selects('tour', "WHERE slider = 1 ORDER BY RAND() LIMIT 6", '');
                        $ti = 0;
                        foreach ($tour_r1 as $tr) {
                            $url = "tour-" . $tr['id'] . "-" . replace_blank($tr['name']) . "";
                            $ti++;
                    ?>
                        <div class="col-md-4 col-sm-6 text-center fh5co-project animate-box" data-animate-effect="fadeIn">
                            <a href="<?= $url ?>" >
                                <img src="images/tours/<?= $tr['img_home'] ?>" alt="Can not find image Happy Taxi Phuket" class="img-responsive ismax">  
                                <span><?= $tr['name'] ?></span>
                            </a>
                        </div>
                    <?php } ?>
                </div>
            </div>
        </div>

		<?php include 'pages/footer.php'; ?>
		<?php include 'inc/jsfoot.inc.php'; ?>
    </body>
</html>